package com.XDApp.xdbase.dao;

import android.content.Context;
import android.content.SharedPreferences;

public class SharePrefUtils {

	public static String getString(Context ctx,String name,String defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		return sp.getString(name, defaultValue);
	}

	public static void setString(Context ctx,String name, String value) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		sp.edit().putString(name, value).commit();
	}
	
	public static Boolean getBoolean(Context ctx,String name,Boolean defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		return sp.getBoolean(name, defaultValue);
	}

	public static void setBoolean(Context ctx,String name, Boolean value) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		sp.edit().putBoolean(name, value).commit();
	}
	
	public static int getInt(Context ctx,String name,int defaultValue) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		return sp.getInt(name, defaultValue);
	}

	public static void setint(Context ctx,String name, int value) {
		SharedPreferences sp = ctx.getSharedPreferences("config",
				Context.MODE_PRIVATE);
		sp.edit().putInt(name, value).commit();
	}

}
