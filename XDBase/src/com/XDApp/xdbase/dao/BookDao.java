package com.XDApp.xdbase.dao;

import com.XDApp.xdbase.dao.base.DAO;
import com.XDApp.xdbase.dao.domain.Book;


public interface BookDao extends DAO<Book> {

}
