package com.XDApp.xdbase.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	private static final String NAME = "ithm.db";
	private static final int VERSION = 1;

	public static final String TABLE_ID = "_id";// 通用的主键

	public static final String TABLE_NEWS_NAME = "news";// 新闻表名

	public static final String TABLE_NEWS_TITLE = "title";// 新闻标题
	public static final String TABLE_NEWS_SUMMARY = "summary";// 新闻摘要

	public static final String TABLE_BOOK_NAME = "book";// 书表名
	public static final String TABLE_BOOK_TITLE = "title";// 书标题

	public DBHelper(Context context) {
		super(context, NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// 新闻表
		// 主键 title summary
		db.execSQL("CREATE TABLE " + TABLE_NEWS_NAME + " (" + //
				TABLE_ID + " integer primary key autoincrement, " + //
				TABLE_NEWS_TITLE + " varchar(50), " + //
				TABLE_NEWS_SUMMARY + " VARCHAR(200))"//
		);
		db.execSQL("CREATE TABLE " + TABLE_BOOK_NAME + " (" + //
				TABLE_ID + " integer primary key autoincrement, " + //

//				TABLE_NEWS_TITLE + " varchar(50), " + //

				TABLE_BOOK_TITLE + " VARCHAR(50))"//
		);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

}
