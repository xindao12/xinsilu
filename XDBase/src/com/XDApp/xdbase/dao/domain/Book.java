package com.XDApp.xdbase.dao.domain;

import com.XDApp.xdbase.dao.DBHelper;
import com.XDApp.xdbase.dao.annotation.Column;
import com.XDApp.xdbase.dao.annotation.ID;
import com.XDApp.xdbase.dao.annotation.TableName;


@TableName(DBHelper.TABLE_BOOK_NAME)
public class Book {
	@ID(autoincrement=true)
	@Column(DBHelper.TABLE_ID)
	private int id;
	@Column(DBHelper.TABLE_BOOK_TITLE)
	private String title;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
