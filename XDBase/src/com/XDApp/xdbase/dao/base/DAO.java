package com.XDApp.xdbase.dao.base;

import java.io.Serializable;
import java.util.List;

/**
 * 实体操作的公共接口
 * 
 * @author Administrator
 * 
 */
public interface DAO<M> {
	/**
	 * 增加
	 * 
	 * @param m
	 * @return id
	 */
	long insert(M m);

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	int delete(Serializable id);// 主键：int long String JPA 主键：Serializable

	/**
	 * 更新
	 * 
	 * @param m
	 * @return
	 */
	int update(M m);

	/**
	 * 查询全部
	 * 
	 * @return
	 */
	List<M> findAll();

	/**
	 * 按照条件查询
	 * 
	 * @param selection
	 * @param selectionArgs
	 * @param orderBy
	 * @return
	 */
	List<M> findByCondition(String selection, String[] selectionArgs, String orderBy);

	/**
	 * 按照条件查询
	 * 
	 * @param columns
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return
	 */
	List<M> findByCondition(String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy);

	// public M getInstance();

}
