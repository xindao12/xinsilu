package com.XDApp.xdbase.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import cn.jpush.android.api.JPushInterface;

import com.XDApp.xdbase.manager.PushMessageManager;
import com.XDApp.xdbase.utils.Logger;
import com.XDApp.xdbase.xinsilu.R;

/**
 * 推送基类
 * 
 * @author ZhuLongLong
 * 
 */
public abstract class BasePushReceiver extends BroadcastReceiver {

	private static final String TAG = "BasePushReceiver";

	private static final int NOTIFICATION_ICON = R.drawable.ic_launcher;

	private final String TARGET_PACKNAME = "com.XDApp.xdbase";

	private final String TARGET_CLASS_NAME = "com.XDApp.xdbase.activitys.MainActivity";

	private NotificationManager nm;

	public BasePushReceiver() {
	}

//	protected BasePushReceiver(String TARGET_PACKNAME, String TARGET_CLASS_NAME) {
//		
//		this.TARGET_PACKNAME = TARGET_PACKNAME;
//		this.TARGET_CLASS_NAME = TARGET_CLASS_NAME;
//		
//	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (null == nm) {
			nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		}

		Bundle bundle = intent.getExtras();
		Logger.d(TAG, "onReceive - " + intent.getAction() + ", extras: ");

		if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
			Logger.d(TAG, "JPush用户注册成功");

		} else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
			Logger.d(TAG, "接受到推送下来的自定义消息");

			// Push Talk messages are push down by custom message format
			// processCustomMessage(context, bundle);

		} else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
			Logger.d(TAG, "接受到推送下来的通知");

//			ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//			
//			/*******************************/
//			 List<ActivityManager.RunningTaskInfo> runningTasks = manager.getRunningTasks(5);  
//		        for(ActivityManager.RunningTaskInfo taskInfo:runningTasks){  
//		        	Logger.i(TAG, taskInfo.baseActivity.getClassName()+"");
//		        } 
//			/*******************************/
			String notificationId;
			notificationId = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_ID);
			String notificationTitle;
			notificationTitle = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);
			String notificationArg0;
			notificationArg0 = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_DEVELOPER_ARG0);
			String contextMessage = bundle.getString(JPushInterface.EXTRA_ALERT);
//			String string = bundle.getString(JPushInterface.EXTRA_EXTRA);
			Logger.i(TAG, contextMessage);
//			String contextMessage = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//			String contextMessage = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//			String contextMessage = bundle.getString(JPushInterface.EXTRA_MESSAGE);
			Boolean onReceiveNotification = false;
			onReceiveNotification = onReceiveNotification(context, nm, notificationId, notificationTitle,
					contextMessage, notificationArg0);

			/************************通知 收到数据*********************/
			Logger.i(TAG, "通知观察者");
			PushMessageManager.getInstrance().setChanged();
			PushMessageManager.getInstrance().notifyObservers();
			/********************************************************/
			if (onReceiveNotification) {
				return;
			}
			ShowNotification(context, nm, notificationId, notificationTitle, contextMessage, notificationArg0);
		} else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
			Logger.d(TAG, "用户点击打开了通知");
		} else {
			Logger.d(TAG, "Unhandled intent - " + intent.getAction());
		}
	}

	/**
	 * 默认notification
	 * 
	 * @param nm
	 * @param notificationId
	 * @param notificationTitle
	 * @param contextMessage
	 * @param notificationArg0
	 */
	@SuppressWarnings("deprecation")
	protected void ShowNotification(Context context, NotificationManager nm, String notificationId,
			String notificationTitle, String contextMessage, String notificationArg0) {

		Notification notification = new Notification(NOTIFICATION_ICON, contextMessage,
				System.currentTimeMillis());
		Intent intent = new Intent();
		intent.setClassName(TARGET_PACKNAME, TARGET_CLASS_NAME);

//		intent.setAction(Intent.FLAG_ACTIVITY_NEW_TASK);
//		intent.putExtra(ConstantValue.NOTIFICATION_TITLE, notificationTitle);
//		intent.putExtra(ConstantValue.NOTIFICATION_CONTEXT, contextMessage);

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, intent,
				PendingIntent.FLAG_ONE_SHOT);
		notification.setLatestEventInfo(context, notificationTitle, contextMessage, pendingIntent);
		notification.flags = Notification.FLAG_AUTO_CANCEL; // 设置通知点击一次后自动清除

		int id;
		if (notificationId == null) {
			id = 5;
		} else {
			id = 0;
		}
		// 用管理器发送通知
		JPushInterface.clearAllNotifications(context);
		nm.notify(id, notification);
	}

	/**
	 * 收到通知后启动的回调。如果不处理直接返回false，将调用默认的Notification样式来显示。若用自定义样式，则处理，最后返回true。
	 * 
	 * @param context
	 * @param nm
	 * @param notificationId
	 *            通知的id
	 * @param notificationTitle
	 *            收到通知后再应用名后（），括号中的内容
	 * @param contextMessage
	 *            消息具体内容
	 * @param notificationArg0
	 * @return
	 */
	public abstract Boolean onReceiveNotification(Context context, NotificationManager nm,
			String notificationId, String notificationTitle, String contextMessage, String notificationArg0);
}
