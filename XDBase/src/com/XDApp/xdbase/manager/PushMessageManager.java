package com.XDApp.xdbase.manager;

import java.util.Observable;

import android.content.Context;

import cn.jpush.android.api.JPushInterface;

public class PushMessageManager extends Observable {

	protected static final String TAG = "BottomManager";
	/******************* 管理对象的创建(单例模式) ***************************************************/
	// 创建一个静态实例
	private static PushMessageManager instrance;

	// 构造私有
	private PushMessageManager() {
	}

	// 提供统一的对外获取实例的入口
	public static PushMessageManager getInstrance() {
		if (instrance == null) {
			instrance = new PushMessageManager();
		}
		return instrance;
	}

	/*********************************************************************************************/
	
	@Override
	public void setChanged() {
		super.setChanged();
	}
	
	/**
	 * 停止服务
	 * @param context
	 */
	public void stopPush(Context context) {
		JPushInterface.stopPush(context);
	}
	
	/**
	 * 开启/回复服务
	 * @param context
	 */
	public void resumePush(Context context) {
		JPushInterface.resumePush(context);
	}
}
