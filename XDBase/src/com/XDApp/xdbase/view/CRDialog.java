package com.XDApp.xdbase.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.XDApp.xdbase.xinsilu.R;

/**
 * 普通dialog
 * 
 * @author xindaoapp
 * 
 */
public class CRDialog extends Dialog implements android.view.View.OnClickListener {

	private Button confirmBtn;
	private Button cancelBtn;
	private TextView msgTv;

	private String msg;
	private String confirmText;
	private String cancelText;

	private OnCheckedListener onCheckedListener;

	public CRDialog(Context context) {
		super(context, R.style.cr_dialog);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cr_dialog);

		setCanceledOnTouchOutside(false);

		confirmBtn = (Button) findViewById(R.id.btn_confirm);
		confirmBtn.setOnClickListener(this);
		cancelBtn = (Button) findViewById(R.id.btn_cancel);
		cancelBtn.setOnClickListener(this);
		msgTv = (TextView) findViewById(R.id.tv_msg);
		if (!TextUtils.isEmpty(cancelText)) {
			confirmBtn.setText(cancelText);
		}
		if (!TextUtils.isEmpty(confirmText)) {
			cancelBtn.setText(confirmText);
		}
		msgTv.setText(msg);
	}

	public void show(String msg, String confirmText, String cancelText) {
		this.msg = msg;
		this.confirmText = confirmText;
		this.cancelText = cancelText;
		show();
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();

		if (id == cancelBtn.getId()) {
			// 取消
			if (onCheckedListener != null) {
				onCheckedListener.onCancel();
			}
			this.dismiss();
		} else if (id == confirmBtn.getId()) {
			// 确定
			if (onCheckedListener != null) {
				onCheckedListener.onConfirm();
			}
			this.dismiss();
		}
	}

	public interface OnCheckedListener {
		/**
		 * 确定
		 */
		public void onConfirm();

		/**
		 * 取消
		 */
		public void onCancel();
	}

	public void removeOnCheckedListener() {
		this.onCheckedListener = null;
	}

	public void setOnCheckedListener(OnCheckedListener onCheckedListener) {
		this.onCheckedListener = onCheckedListener;
	}
}
