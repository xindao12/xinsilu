package com.XDApp.xdbase.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.XDApp.xdbase.intf.OnToggleStateChangeListener;

public class ToggleView extends View {

	private boolean currentState = false; // 控制开关的状态
	private Bitmap switchBackgroundBitmap; // 开关的背景图片
	private Bitmap slideBtnBackgroundBitmap; // 开关的滑动按钮图片
	private Rect rectON; // 开关开启状态的矩形
	private Rect rectOFF; // 开关关闭状态的矩形
	private int currentX = 0; // 当前手指按下的x轴的偏移量
	private boolean isSliding = false; // 是否正在滑动中
	private OnToggleStateChangeListener mOnToggleStateChangeListener; // 开关状态的监听事件

	public ToggleView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * 需要得到控件的宽和高
	 */
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		// 设置开关的宽和高为背景图片的宽高
		setMeasuredDimension(switchBackgroundBitmap.getWidth(),
				switchBackgroundBitmap.getHeight());
	}

	/**
	 * 绘制控件时调用, invalidate() 手动也可以触发此方法
	 * 
	 * @param canvas
	 *            画板
	 */
	@Override
	protected void onDraw(Canvas canvas) {
		// 绘制背景图片
		canvas.drawBitmap(switchBackgroundBitmap, 0, 0, null);

		// 绘制滑动按钮的位置

		if (isSliding) {
			// 正在滑动中

			// 计算出滑动按钮左边的偏移量
			int left = currentX - slideBtnBackgroundBitmap.getWidth() / 2;

			if (left < rectOFF.left) {
				left = 0; // 超出了背景图片的左边界
			} else if (left > rectON.left) {
				left = rectON.left; // 滑动按钮的右边超出了背景图片的右边界
			}

			canvas.drawBitmap(slideBtnBackgroundBitmap, left, 0, null);
		} else {
			if (currentState) { // 绘制为打开的状态
				canvas.drawBitmap(slideBtnBackgroundBitmap, rectON.left,
						rectON.top, null);
			} else { // 绘制为关闭的状态
				canvas.drawBitmap(slideBtnBackgroundBitmap, rectOFF.left,
						rectOFF.top, null);
			}
		}
		super.onDraw(canvas);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			isSliding = true;
			currentX = (int) event.getX();
			break;
		case MotionEvent.ACTION_MOVE:
			currentX = (int) event.getX();
			break;
		case MotionEvent.ACTION_UP:
			isSliding = false;
			currentX = (int) event.getX();

			// 计算滑动按钮的中心点是否大于背景图片的中心点, 如果大于, 置为开的状态

			boolean state = currentX > switchBackgroundBitmap.getWidth() / 2;

			if (currentState != state // 如果前一个开关的状态不等于现在的状态就进入if
					&& mOnToggleStateChangeListener != null) {
				mOnToggleStateChangeListener.onToggleStateChanged(state);
			}
			currentState = state;
			break;
		default:
			break;
		}
		// System.out.println("x = " + currentX);
		invalidate(); // 通知View的onDraw方法绘制控件
		return true;
	}

	/**
	 * 设置开关的图片资源
	 * 
	 * @param switchBackground
	 *            背景图片
	 * @param slideButtonBackground
	 *            滑动按钮的图片
	 */
	public void setImageBackground(int switchBackground,
			int slideButtonBackground) {
		// 背景图片
		switchBackgroundBitmap = BitmapFactory.decodeResource(getResources(),
				switchBackground);
		// 滑动按钮图片
		slideBtnBackgroundBitmap = BitmapFactory.decodeResource(getResources(),
				slideButtonBackground);

		// 开关开启的矩形
		rectON = new Rect(switchBackgroundBitmap.getWidth()
				- slideBtnBackgroundBitmap.getWidth(), 0,
				switchBackgroundBitmap.getWidth(),
				switchBackgroundBitmap.getHeight());

		// 开关的关闭的矩形
		rectOFF = new Rect(0, 0, slideBtnBackgroundBitmap.getWidth(),
				switchBackgroundBitmap.getHeight());
	}

	/**
	 * 设置开关的状态
	 * 
	 * @param state
	 */
	public void setCurrentToggleState(boolean state) {
		this.currentState = state;
	}

	/**
	 * 设置当前开关的监听事件
	 * 
	 * @param listener
	 */
	public void setOnToggleStateChangeListener(
			OnToggleStateChangeListener listener) {
		this.mOnToggleStateChangeListener = listener;
	}

	public OnToggleStateChangeListener getOnToggleStateChangeListener() {
		return mOnToggleStateChangeListener;
	}
}
