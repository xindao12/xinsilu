package com.XDApp.xdbase.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.xinsilu.R;

/**
 * 自定义吐司
 * 
 * @author xindaoapp
 * 
 */
public class CRToast extends Toast {
	private final Context context;

	public CRToast(Context context) {
		super(context);
		this.setGravity(Gravity.CENTER, 0, 0);
		this.context = context;
	}

	/**
	 * 显示一个吐司
	 * @param context 上下文
	 * @param txt 要显示的文字
	 */
	public static void show(Context context, String txt) {
		CRToast.show(context, txt, null);
	}

	/**
	 * 显示一个吐司
	 * @param context  上下文对象
	 * @param txt 需要显示的文本
	 * @param imgIc 图标
	 */
	public static void show(Context context, String txt, Drawable imgIc) {
		if (txt == null)
			return;

		CRToast toast = new CRToast(context);
		if (txt.length() < 25) {
			toast.setDuration(LENGTH_SHORT);
		} else {
			toast.setDuration(LENGTH_LONG);
		}
		toast.createView(txt, imgIc);

		toast.show();
	}

	/**'
	 * 创建一个吐司的View
	 * @param txt 
	 * @param imgIc
	 */
	public void createView(String txt, Drawable imgIc) {
		View view = View.inflate(this.context, R.layout.layout_toast, null);
		TextView txtTv = (TextView) view.findViewById(R.id.tv_toast_txt);

		txtTv.setText(txt);

		if (imgIc != null) {
			ImageView imgIv = (ImageView) view.findViewById(R.id.iv_msg_ic);
			imgIv.setImageDrawable(imgIc);
			imgIv.setVisibility(View.VISIBLE);
		}

		setView(view);
	}
}
