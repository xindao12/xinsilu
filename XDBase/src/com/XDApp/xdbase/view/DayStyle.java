package com.XDApp.xdbase.view;

import java.util.Calendar;

import android.graphics.Color;

public class DayStyle {

	// methods
	private static String[] getWeekDayNames() {
		String[] vec = new String[10];

		vec[Calendar.SUNDAY] = "周日";
		vec[Calendar.MONDAY] = "周一";
		vec[Calendar.TUESDAY] = "周二";
		vec[Calendar.WEDNESDAY] = "周三";
		vec[Calendar.THURSDAY] = "周四";
		vec[Calendar.FRIDAY] = "周五";
		vec[Calendar.SATURDAY] = "周六";
		return vec;
	}

	public static String getWeekDayName(int iDay) {
		return vecStrWeekDayNames[iDay];
	}

	// fields
	private final static String[] vecStrWeekDayNames = getWeekDayNames();

	// fields
	public final static int iColorFrameHeader = 0xff666666;
	public final static int iColorFrameHeaderHoliday = 0xff707070;
	public final static int iColorTextHeader = 0xffcccccc;

	public final static int iColorText = 0xff000000;
	public final static int iColorBkg = 0xff888888;
	public final static int iColorBkgHoliday = 0xffaaaaaa;

	public final static int iColorTextToday = 0xff002200;
	public final static int iColorBkgToday = 0xff88bb88;
	public final static int iColorBkgTomAndAfter = 0xff32bbdc;

	public final static int iColorTextSelected = 0xff001122;
	public final static int iColorBkgSelectedLight = 0xffbbddff;
	public final static int iColorBkgSelectedDark = 0xff225599;

	public final static int iColorTextFocused = 0xff221100;
	public final static int iColorBkgFocusLight = 0xffffddbb;
	public final static int iColorBkgFocusDark = 0xffaa5500;

	// methods
	public static int getColorFrameHeader(boolean bHoliday) {
		if (bHoliday) {
			return iColorFrameHeaderHoliday;
		}
		return iColorFrameHeader;
	}

	public static int getColorTextHeader(boolean bHoliday, int iWeekDay) {
		if (bHoliday) {
			if (iWeekDay == Calendar.SATURDAY)
				return Color.BLUE;
			else
				return Color.RED;
		}
		return iColorTextHeader;
	}

	public static int getColorText(boolean bHoliday, boolean bToday, int iDayOfWeek, boolean bTomorrow,
			boolean bBeTom) {
		if (bToday) {
			return Color.WHITE;
		}
		if (bTomorrow || bBeTom)
			return iColorBkgTomAndAfter;
		if (bHoliday) {
			if (iDayOfWeek == Calendar.SATURDAY) {
				return Color.BLUE;
			} else
				return Color.RED;
		}
		return iColorText;
	}

	public static int getColorText(boolean bHoliday, boolean bToday, int iDayOfWeek) {
		if (bToday)
//			return iColorBkgTomAndAfter;
			return Color.WHITE;
		if (bHoliday) {
			if (iDayOfWeek == Calendar.SATURDAY) {
				return Color.BLUE;
			} else
				return Color.RED;
		}
		return iColorText;
	}

	public static int getColorBkg(boolean bHoliday, boolean bToday) {
		if (bToday)
//			return iColorBkgToday;
			return Color.WHITE;
		if (bHoliday)
			return iColorBkgHoliday;
		return iColorBkg;
	}

	public static int getWeekDay(int index, int iFirstDayOfWeek) {
		int iWeekDay = -1;
		if (iFirstDayOfWeek == Calendar.MONDAY) {
			iWeekDay = index + Calendar.MONDAY;
			if (iWeekDay > Calendar.SATURDAY)
				iWeekDay = Calendar.SUNDAY;
		}
		if (iFirstDayOfWeek == Calendar.SUNDAY) {
			iWeekDay = index + Calendar.SUNDAY;
		}
		return iWeekDay;
	}

}
