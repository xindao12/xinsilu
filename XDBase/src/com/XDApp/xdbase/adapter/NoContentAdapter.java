package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.FreeWalkerAndToursActivity;
import com.XDApp.xdbase.activitys.FreeWalkerDetailActivity;
import com.XDApp.xdbase.bean.FreeWalker;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;
import com.squareup.picasso.Picasso;

public class NoContentAdapter extends XinDaoBaseAdapter<FreeWalker> {
	public NoContentAdapter(Context context, List<FreeWalker> iniData, int pageSize, int res, int loadingRes,
			String flag) {
		super(context, iniData, pageSize, res, loadingRes);
		this.mContext = context;
		this.flag = flag;
	}

	private final Context mContext;
	private final String flag;

	class ViewHolder {
		private ImageView headIcon;
		private TextView title;
		private TextView normalPrice;
		private TextView vipPrice;
		private TextView introduce;
	}

	@Override
	public void nextPage(int start, int size,
			com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<FreeWalker> iLoadNextPageData) {
		FreeWalkerAndToursActivity.getData(mContext, flag, start, size, iLoadNextPageData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, final FreeWalker fw) {
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
//			convertView = View.inflate(mContext, R.layout.activity_freewalker_item, null);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
			holder.title = (TextView) convertView.findViewById(R.id.tv_freewalker_item_title);
			holder.headIcon = (ImageView) convertView.findViewById(R.id.iv_headicon);
			holder.introduce = (TextView) convertView.findViewById(R.id.tv_introduce);
			convertView.setTag(holder);
		}
		holder.introduce.setText(fw.getIntroduce());
		holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		holder.normalPrice.setText("￥" + fw.getNormalPrice());
		holder.vipPrice.setText("￥" + fw.getVipPrice());
		holder.headIcon.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		holder.headIcon.setScaleType(ScaleType.FIT_XY);
		if (TextUtils.isEmpty(fw.getUrl())) {
			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
		} else {
			Picasso.with(mContext).load(fw.getUrl()).placeholder(R.drawable.about_headicon)
					.into(holder.headIcon);
		}
		holder.title.setText(fw.getTitle());
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, FreeWalkerDetailActivity.class);
				intent.putExtra("seneryID", fw.getSeneryId());
				intent.putExtra("flag", flag);
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

}
