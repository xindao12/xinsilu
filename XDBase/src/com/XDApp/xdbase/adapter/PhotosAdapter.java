package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.XDApp.xdbase.xinsilu.R;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PhotosAdapter extends PagerAdapter {
	private final List<String> list;
	private final Context mContext;

	public PhotosAdapter(Context context, List<String> list) {
		super();
		this.list = list;
		this.mContext = context;
	}

	@Override
	public int getCount() {
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView view = (ImageView) View.inflate(mContext, R.layout.item_detail_photo, null);
		ImageLoader.getInstance().displayImage(list.get(position % list.size()), view);
		container.addView(view);
		return view;
	}
}
