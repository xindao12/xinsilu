package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.MyCommentActivity;
import com.XDApp.xdbase.bean.CommentList;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;
import com.squareup.picasso.Picasso;

public class MyCommentAdapter extends XinDaoBaseAdapter<CommentList> {
	private final int type;

	public MyCommentAdapter(int type, Context context, List<CommentList> iniData, int pageSize, int res,
			int loadingRes) {
		super(context, iniData, pageSize, res, loadingRes);
		this.type = type;
	}

	class ViewHolder {
		ImageView headicon;
		TextView title;
		TextView content;
		TextView vipPrice;
		TextView normalPrice;
		TextView comment;
	}

	@Override
	public void nextPage(int start, int size,
			com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<CommentList> iLoadNextPageData) {
		MyCommentActivity.initData(mContext, start, size, iLoadNextPageData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, CommentList list) {
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
//			convertView = View.inflate(mContext, R.layout.listview_myorder_item, null);
			convertView.setTag(holder);
			holder.headicon = (ImageView) convertView.findViewById(R.id.iv_headicon);
			holder.title = (TextView) convertView.findViewById(R.id.tv_mycomment_title);
			holder.content = (TextView) convertView.findViewById(R.id.tv_mycomment_content);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
			holder.comment = (TextView) convertView.findViewById(R.id.tv_comment);
		}
		if (null != list) {
			if (type == 0) {// 未支付
				holder.comment.setText("去点评");
				holder.comment.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources()
						.getDrawable(R.drawable.right_arrow), null);
				holder.comment.setTextColor(Color.parseColor("#5FCE67"));
			} else if (type == 1) {// 已支付
				holder.comment.setText("已点评");
			}
			holder.headicon.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.FILL_PARENT));
			holder.headicon.setScaleType(ScaleType.FIT_XY);
			if (TextUtils.isEmpty(list.getUrl())) {
				holder.headicon.setBackgroundResource(R.drawable.about_headicon);
			} else {
				Picasso.with(mContext).load(list.getUrl()).placeholder(R.drawable.about_headicon)
						.into(holder.headicon);
			}

			holder.title.setText(list.getTitle());
			holder.content.setText(list.getIntroduce());
			holder.vipPrice.setText(list.getVipPrice());
			holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
			holder.normalPrice.setText(list.getNormalPrice());
		} else {
			holder.headicon.setBackgroundResource(0);
			holder.title.setText("");
			holder.content.setText("");
			holder.vipPrice.setText("");
			holder.normalPrice.setText("");
		}
		return convertView;
	}
}
