package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.BaseObjectListAdapter;
import com.XDApp.xdbase.bean.BaseEntity;
import com.XDApp.xdbase.bean.PersonInfo;
import com.XDApp.xdbase.xinsilu.R;

public class PersonAdapter extends BaseObjectListAdapter {

	public PersonAdapter(BaseApplication application, Context context, List<? extends BaseEntity> datas) {
		super(application, context, datas);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();

			holder.icon = (ImageView) convertView.findViewById(R.id.iv_icon);
			holder.title = (TextView) convertView.findViewById(R.id.tv_item_title);
			holder.desc = (TextView) convertView.findViewById(R.id.tv_item_desc);
			holder.time = (TextView) convertView.findViewById(R.id.tv_item_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		PersonInfo person = (PersonInfo) getItem(position);
//		holder.mIvAvatar.setImageBitmap(mApplication.getAvatar(people
//				.getAvatar()));
		holder.icon.setImageBitmap(mApplication.getAvatar(person.getPic()));
		holder.title.setText(person.getTitle());
		holder.desc.setText(person.getDescription());
		holder.time.setText(person.getTime());
		return convertView;
	}

	class ViewHolder {

		ImageView icon;
		TextView title;
		TextView desc;
		TextView time;

	}
}
