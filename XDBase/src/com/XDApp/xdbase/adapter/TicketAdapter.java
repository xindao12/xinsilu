package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.TicketActivity;
import com.XDApp.xdbase.activitys.TicketDetailActivity;
import com.XDApp.xdbase.bean.Ticket;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;
import com.squareup.picasso.Picasso;

public class TicketAdapter extends XinDaoBaseAdapter<Ticket> {
	public TicketAdapter(Context context, List<Ticket> iniData, int pageSize, int res, int loadingRes) {
		super(context, iniData, pageSize, res, loadingRes);
		mContext = context;
	}

	public void setLists(List<Ticket> lists) {
		this.lists = lists;
	}

	private List<Ticket> lists;
	private final Context mContext;

	/*public TicketAdapter(Context mContext,List<Ticket> lists) {
		super();
		this.lists=lists;
		this.mContext=mContext;
	}*/

	/*@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		final Ticket tk = lists.get(position);
		if (null == convertView) {
			holder = new ViewHolder();
			convertView = View.inflate(mContext, R.layout.activity_ticket_item, null);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
			holder.title = (TextView) convertView.findViewById(R.id.tv_freewalker_item_title);
			holder.headIcon = (ImageView) convertView.findViewById(R.id.iv_headicon);
			holder.introduce = (TextView) convertView.findViewById(R.id.tv_introduce);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.introduce.setText(tk.getSeneryLocation());
		holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		holder.normalPrice.setText("￥" + tk.getNormalPrice());
		holder.vipPrice.setText("￥" + tk.getVipPrice());
		if (TextUtils.isEmpty(tk.getSeneryUrl())) {
			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
		} else {
			Picasso.with(mContext).load(tk.getSeneryUrl()).placeholder(R.drawable.about_headicon)
					.into(holder.headIcon);
		}
		holder.title.setText(tk.getSeneryTitle());
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, TicketDetailActivity.class);
				intent.putExtra("ticket_id", tk.getTicketId());
				intent.putExtra("ticket_name", tk.getSeneryTitle());
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}*/

	class ViewHolder {
		private ImageView headIcon;
		private TextView title;
		private TextView normalPrice;
		private TextView vipPrice;
		private TextView introduce;
	}

	@Override
	public void nextPage(int start, int size,
			com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<Ticket> iLoadNextPageData) {
		TicketActivity.getData(mContext, start, size, iLoadNextPageData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, final Ticket tk) {
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
//			convertView = View.inflate(mContext, R.layout.activity_ticket_item, null);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
			holder.title = (TextView) convertView.findViewById(R.id.tv_freewalker_item_title);
			holder.headIcon = (ImageView) convertView.findViewById(R.id.iv_headicon);
			holder.introduce = (TextView) convertView.findViewById(R.id.tv_introduce);
			convertView.setTag(holder);
		}
		holder.introduce.setText(tk.getSeneryLocation());
		holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		holder.normalPrice.setText("￥" + tk.getNormalPrice());
		holder.vipPrice.setText("￥" + tk.getVipPrice());
		holder.headIcon.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		holder.headIcon.setScaleType(ScaleType.FIT_XY);
		if (TextUtils.isEmpty(tk.getSeneryUrl())) {
			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
		} else {
			Picasso.with(mContext).load(tk.getSeneryUrl()).placeholder(R.drawable.about_headicon)
					.into(holder.headIcon);
		}
		holder.title.setText(tk.getSeneryTitle());
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, TicketDetailActivity.class);
				intent.putExtra("ticket_id", tk.getTicketId());
				intent.putExtra("ticket_name", tk.getSeneryTitle());
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

}
