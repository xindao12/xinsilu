package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.MyOrderActivity;
import com.XDApp.xdbase.bean.OrderList;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;

public class MyOrderAdapter extends XinDaoBaseAdapter<OrderList> {
	private final int type;

	public MyOrderAdapter(Context context, int type, List<OrderList> iniData, int pageSize, int res,
			int loadingRes) {
		super(context, iniData, pageSize, res, loadingRes);
		this.type = type;
	}

	class ViewHolder {
		TextView orderId;
		TextView orderTime;
		TextView orderTitle;
		TextView orderPrice;
		TextView playTime;
		Button order;
	}

	@Override
	public void nextPage(int start, int size,
			com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<OrderList> iLoadNextPageData) {
		MyOrderActivity.initData(mContext, start, size, iLoadNextPageData);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, OrderList list) {
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
			holder.orderId = (TextView) convertView.findViewById(R.id.tv_order_tel);
			holder.orderTime = (TextView) convertView.findViewById(R.id.tv_order_time);
			holder.orderTitle = (TextView) convertView.findViewById(R.id.tv_name);
			holder.orderPrice = (TextView) convertView.findViewById(R.id.tv_price);
			holder.playTime = (TextView) convertView.findViewById(R.id.tv_tour_time);
			holder.order = (Button) convertView.findViewById(R.id.btn_goto_pay);
			convertView.setTag(holder);
		}
		if (null != list) {
			holder.orderId.setText(list.getOrderId());
			holder.orderTime.setText(list.getOrderTime());
			holder.orderTitle.setText(list.getOrderName());
			holder.orderPrice.setText(String.format("￥ %s * %s", list.getOrderPrice(), list.getNums()));
			holder.playTime.setText("游玩时间:" + list.getOrderPalyTime());
			if (type == 1) {
				holder.order.setText("已支付");
			}
		} else {
			holder.orderId.setText("");
			holder.orderTime.setText("");
			holder.orderTitle.setText("");
			holder.orderPrice.setText("");
			holder.playTime.setText("");
		}
		return convertView;
	}
}
