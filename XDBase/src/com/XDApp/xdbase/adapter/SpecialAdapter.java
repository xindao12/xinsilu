package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.SeneryDetailActivity;
import com.XDApp.xdbase.bean.Product;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;
import com.squareup.picasso.Picasso;

public class SpecialAdapter extends XinDaoBaseAdapter<Product> {
	private final Context mContext;

	public SpecialAdapter(Context context, List<Product> iniData, int pageSize, int res, int loadingRes) {
		super(context, iniData, pageSize, res, loadingRes);
		mContext = context;
	}

//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		ViewHolder holder;
//		if (null == convertView) {
//			holder = new ViewHolder();
//			convertView = View.inflate(mContext, R.layout.activity_freewalker_item, null);
//			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
//			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
//			holder.title = (TextView) convertView.findViewById(R.id.tv_freewalker_item_title);
//			holder.headIcon = (ImageView) convertView.findViewById(R.id.iv_headicon);
//			convertView.setTag(holder);
//		} else {
//			holder = (ViewHolder) convertView.getTag();
//		}
//		holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
//		holder.normalPrice.setText("￥" + fw.getNormalPrice());
//		holder.vipPrice.setText("￥" + fw.getVipPrice());
//		if (TextUtils.isEmpty(fw.getProductUrl())) {
//			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
//		} else {
//			Picasso.with(mContext).load(fw.getProductUrl()).placeholder(R.drawable.about_headicon)
//					.into(holder.headIcon);
//		}
//		holder.title.setText(fw.getProductTitle());
//		convertView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(mContext, SeneryDetailActivity.class);
//				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTID, fw.getProductId());
//				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTNAME, fw.getProductTitle());
//				mContext.startActivity(intent);
//			}
//		});
//		return convertView;
//	}
	class ViewHolder {
		private ImageView headIcon;
		private TextView title;
		private TextView normalPrice;
		private TextView vipPrice;
	}

	@Override
	public void nextPage(int start, int size,
			com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<Product> iLoadNextPageData) {

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent, final Product fw) {
		ViewHolder holder = (ViewHolder) convertView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
//			convertView = View.inflate(mContext, R.layout.activity_freewalker_item, null);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_normalPrice);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_vipPrice);
			holder.title = (TextView) convertView.findViewById(R.id.tv_freewalker_item_title);
			holder.headIcon = (ImageView) convertView.findViewById(R.id.iv_headicon);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
		holder.normalPrice.setText("￥" + fw.getNormalPrice());
		holder.vipPrice.setText("￥" + fw.getVipPrice());
		holder.headIcon.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		holder.headIcon.setScaleType(ScaleType.FIT_XY);
		if (TextUtils.isEmpty(fw.getProductUrl())) {
			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
		} else {
			Picasso.with(mContext).load(fw.getProductUrl()).placeholder(R.drawable.about_headicon)
					.into(holder.headIcon);
		}
		holder.title.setText(fw.getProductTitle());
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, SeneryDetailActivity.class);
				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTID, fw.getProductId());
				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTNAME, fw.getProductTitle());
				mContext.startActivity(intent);
			}
		});
		return convertView;
	}

}
