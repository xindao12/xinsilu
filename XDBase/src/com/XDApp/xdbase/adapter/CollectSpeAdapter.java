package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.SeneryDetailActivity;
import com.XDApp.xdbase.bean.CollectList;
import com.XDApp.xdbase.utils.MyBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;
import com.squareup.picasso.Picasso;

/** 我的收藏 特产*/
public class CollectSpeAdapter extends MyBaseAdapter<CollectList> {

	private final Context mContext;
	private final List<CollectList> mList;

	public CollectSpeAdapter(Context context, List<CollectList> mList, int resId) {
		super(context, mList, resId);
		this.mContext = context;
		this.mList = mList;
	}

	public void addData(List<CollectList> mList) {
		this.mList.addAll(mList);
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View contentView, ViewGroup parent, final CollectList t) {
		ViewHolder holder = (ViewHolder) contentView.getTag();
		if (null == holder) {
			holder = new ViewHolder();
			holder.normalPrice = (TextView) contentView.findViewById(R.id.tv_normalPrice);
			holder.vipPrice = (TextView) contentView.findViewById(R.id.tv_vipPrice);
			holder.title = (TextView) contentView.findViewById(R.id.tv_freewalker_item_title);
			holder.headIcon = (ImageView) contentView.findViewById(R.id.iv_headicon);
			contentView.setTag(holder);
		}

		String str = "";

//		str = t.getIntroduce();
//		if (!TextUtils.isEmpty(str)) {
//			holder.introduce.setText(str);
//		}

		str = t.getNormalPrice();
		if (!TextUtils.isEmpty(str)) {
			holder.normalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
			holder.normalPrice.setText("￥" + str);
		}

		str = t.getVipPrice();
		if (!TextUtils.isEmpty(str)) {
			holder.vipPrice.setText("￥" + str);
		}

		str = t.getTitle();
		if (!TextUtils.isEmpty(str)) {
			holder.title.setText(str);
		}

		str = t.getUrl();
		if (TextUtils.isEmpty(str)) {
			holder.headIcon.setBackgroundResource(R.drawable.about_headicon);
		} else {
			Picasso.with(mContext).load(str).placeholder(R.drawable.about_headicon).into(holder.headIcon);
		}

		contentView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, SeneryDetailActivity.class);
				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTID, t.getSeneryId());
				intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTNAME, t.getTitle());
				mContext.startActivity(intent);
			}
		});
		return contentView;

	}

	class ViewHolder {
		private ImageView headIcon;
		private TextView title;
		private TextView normalPrice;
		private TextView vipPrice;
//		private TextView introduce;
	}

}
