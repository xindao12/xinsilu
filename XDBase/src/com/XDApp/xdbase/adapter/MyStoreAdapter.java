package com.XDApp.xdbase.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.bean.CollectList;
import com.XDApp.xdbase.xinsilu.R;

public class MyStoreAdapter extends BaseAdapter {
	private final Context mContext;
	private List<CollectList> storeLists;

	public MyStoreAdapter(Context mContext, List<CollectList> storeLists) {
		super();
		this.mContext = mContext;
		this.storeLists = storeLists;
	}

	public void setStoreLists(List<CollectList> storeLists) {
		this.storeLists = storeLists;
	}

	@Override
	public int getCount() {
		return null != storeLists ? storeLists.size() : 0;
	}

	@Override
	public Object getItem(int position) {
		return storeLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (null == convertView) {
			holder = new ViewHolder();
			convertView = View.inflate(mContext, R.layout.listview_mystore_item, null);
			convertView.setTag(holder);
			holder.title = (TextView) convertView.findViewById(R.id.tv_title);
			holder.introduce = (TextView) convertView.findViewById(R.id.tv_address);
			holder.url = (ImageView) convertView.findViewById(R.id.iv_url);
			holder.normalPrice = (TextView) convertView.findViewById(R.id.tv_previous_price);
			holder.vipPrice = (TextView) convertView.findViewById(R.id.tv_present_price);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CollectList list = storeLists.get(position);
		if (null != list) {
			holder.title.setText(list.getTitle());
			holder.introduce.setText(list.getIntroduce());
			holder.vipPrice.setText(list.getVipPrice() + "起");
			holder.normalPrice.setText(list.getNormalPrice());

		} else {

		}
		return null;
	}

	class ViewHolder {
		TextView title;
		TextView introduce;
		ImageView url;
		TextView normalPrice;
		TextView vipPrice;
	}

}
