package com.XDApp.xdbase.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.activitys.LoginActivity;
import com.XDApp.xdbase.activitys.MyOrderActivity;
import com.XDApp.xdbase.utils.myutils.ImageCache;
import com.XDApp.xdbase.xinsilu.R;

/**
 */
public class OrderFragment extends XDBaseFragment {

	private static final String TAG = "MoreFragment";
	private ListView listview;
	private ImageView ivBack;
	private TextView title;
	String[] str = new String[] { "景点门票订单", "自由行订单", "参团游订单", "特产订单" };

	private BaseApplication mApplication;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(TAG, "onActivityCreated");

		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		Log.i(TAG, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, "onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// getView
		Log.i(TAG, "onCreateView");
		mView = inflater.inflate(R.layout.fragment_order, null);
		initViews();
		return mView;
	}

	private void initViews() {

		mApplication = BaseApplication.getApplication();

		listview = (ListView) findViewById(R.id.listview_order);
		ivBack = (ImageView) findViewById(R.id.title_back);
		ivBack.setVisibility(View.GONE);
		title = (TextView) findViewById(R.id.title_text);
		title.setText("我的订单");
		listview.setAdapter(new ArrayAdapter<String>(mContext, R.layout.listview_orderlist_item,
				R.id.tv_order_name, str));
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

				if (mApplication.isLogin()) {
					Intent intent = new Intent(mContext, MyOrderActivity.class);
					intent.putExtra("type", arg2);
					startActivity(intent);
				} else {
					Intent intent1 = new Intent(mContext, LoginActivity.class);
//					startActivityForResult(intent1, 100);
					startActivity(intent1);
				}

			}
		});
	}

	@Override
	public void onDestroy() {
		Log.i(TAG, "onDestroy");
		ImageCache.getInstance().clear();
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		Log.i(TAG, "onDestroyView");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.i(TAG, "onDetach");
		super.onDetach();
	}

	@Override
	public void onPause() {
		Log.i(TAG, "onPause");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.i(TAG, "onResume");
		super.onResume();
	}

	@Override
	public void onStart() {
		Log.i(TAG, "onStart");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.i(TAG, "onStop");
		super.onStop();
	}

}
