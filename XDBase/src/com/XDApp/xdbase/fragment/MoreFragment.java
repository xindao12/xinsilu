package com.XDApp.xdbase.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.XDApp.xdbase.activitys.AboutActivity;
import com.XDApp.xdbase.activitys.HelpActivity;
import com.XDApp.xdbase.activitys.QuestionActivity;
import com.XDApp.xdbase.xinsilu.R;

public class MoreFragment extends XDBaseFragment implements OnClickListener {

	private static final String TAG = "MoreFragment";
	private View inflate;
	private TextView titleName;
	private ImageView iv_back;
	private RelativeLayout rl_help, rl_question, rl_about, rl_callphone;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		inflate = inflater.inflate(R.layout.fragment_more, container, false);
		return inflate;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		titleName = (TextView) inflate.findViewById(R.id.title_text);
		iv_back = (ImageView) inflate.findViewById(R.id.title_back);
		rl_help = (RelativeLayout) inflate.findViewById(R.id.rl_help);
		rl_question = (RelativeLayout) inflate.findViewById(R.id.rl_question);
		rl_about = (RelativeLayout) inflate.findViewById(R.id.rl_about);
		rl_callphone = (RelativeLayout) inflate.findViewById(R.id.rl_callphone);
		initData();
	}

	private void initData() {
		rl_help.setOnClickListener(this);
		rl_question.setOnClickListener(this);
		rl_about.setOnClickListener(this);
		rl_callphone.setOnClickListener(this);
		titleName.setText("更多");
		iv_back.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rl_help:
			Intent intent1 = new Intent(mContext, HelpActivity.class);
			startActivity(intent1);
			break;
		case R.id.rl_question:
			Intent intent2 = new Intent(mContext, QuestionActivity.class);
			startActivity(intent2);
			break;
		case R.id.rl_about:
			Intent intent3 = new Intent(mContext, AboutActivity.class);
			startActivity(intent3);
			break;
		case R.id.rl_callphone:
			Intent intent4 = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "4006029680"));
			startActivity(intent4);
			break;

		}
	}
}
