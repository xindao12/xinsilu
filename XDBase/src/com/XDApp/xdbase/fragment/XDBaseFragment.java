package com.XDApp.xdbase.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.XDApp.xdbase.utils.CommonLog;
import com.XDApp.xdbase.utils.CommonUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass. Activities that
 * contain this fragment must implement the
 * {@link XDBaseFragment.OnFragmentInteractionListener} interface to handle
 * interaction events. Use the {@link XDBaseFragment#newInstance} factory method
 * to create an instance of this fragment.
 * 
 */
public class XDBaseFragment extends Fragment implements OnCancelListener {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";
	protected View mView;
	protected ImageLoader imageLoader = ImageLoader.getInstance();
	protected static CommonLog log = new CommonLog();
	protected Context mContext;
	protected LayoutInflater mInflater;
	protected SharedPreferences mPreferences;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		mInflater = LayoutInflater.from(mContext);;
		mPreferences = CommonUtil.getSharedPreferences(mContext);
	}
	
	protected View findViewById(int id){
		return mView.findViewById(id);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onCancel(DialogInterface arg0) {
//		mProgressHUD.dismiss();		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		initUi();
		super.onActivityCreated(savedInstanceState);
	}
	
	protected void fragmentResume(){
		
	}
	
	protected void initUi(){
		
	}

	
}
