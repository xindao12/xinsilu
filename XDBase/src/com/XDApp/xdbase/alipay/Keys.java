﻿/*
 * Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 * 
 *  提示：如何获取安全校验码和合作身份者id
 *  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
 *  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
 */

package com.XDApp.xdbase.alipay;

//
// 请参考 Android平台安全支付服务(msp)应用开发接口(4.2 RSA算法签名)部分，并使用压缩包中的openssl RSA密钥生成工具，生成一套RSA公私钥。
// 这里签名时，只需要使用生成的RSA私钥。
// Note: 为安全起见，使用RSA私钥进行签名的操作过程，应该尽量放到商家服务器端去进行。
public final class Keys {

	// 合作身份者id，以2088开头的16位纯数字
	public static final String DEFAULT_PARTNER = "2088211557333620";

	// 收款支付宝账号
	public static final String DEFAULT_SELLER = "silutrip@163.com";

	// 商户私钥，自助生成
	public static final String PRIVATE = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBALX5VUchwwmtTZ/+zlhpqszn0RKqdfy2qUJDAYI0LeNw1O8DZgzmMgS1ajT2naGXR0iN/+9LQm3QqePYx2UuFliiv2frtFM37oQSC7pm1vjk1daKf6P+VlIGQ+ZHXWpOqLIgsD3FMoL/YMqRt0oEX8OpUSnJoDs58DCVhPWwQT7bAgMBAAECgYA/BCvy6Q8of7o86Lk+RsFeGfz90MfxDrRIfPm9xCIL/BvQ9UVWvDSgCs7ProNh3QhTaNEzZcWIT9U1UPGj6TTVQXEkGqg5IlUUC+Zgs38ktRRkHizpQWVWJlA20xCxW8EW0UbN+QomYhNlp+ZAJ0y5o5+ezxAqM8x5bKxfkvHxgQJBANk1blUSX9eZpnB35HOKGMzSLKOMNS8IaVuVZ186WN0fwcFWD6vTMIeqq87YYu1/by/jNOo50XmUH5wx1GuRLkECQQDWeQDvGR16dCEy3t/60IWrsPHw9LmmNF1frTsWkivpo7YVoV2OT5fQi0U5BMfH9nCDNNRjyFq/2CqiYXEGld4bAkAW/psRkoJm9rMyEN80hBggBtgG8F7o/cD5mxV4dVrW/ferGgtKq27lzeGmYi4G6ojAzxLZAQn2DwJRrMzKHnKBAkAC/OKPcikf+JgxzjsOknMaRjfIpJAliZs3ZwFouhaUhaxWts3H2sIBrL8FIVTrKzq8CTNgRH1HKOrHIlZhwhq7AkEAghtloMVK/SERScgUxxUfQoIzouC6bUXI10ombIv8C/GMdOYxD/35PRH0p9j0GSZq+z4gEfU1I0++E7ZMtfRkjA==";

	public static final String PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";

}
