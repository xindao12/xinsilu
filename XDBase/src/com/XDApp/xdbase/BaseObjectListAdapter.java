package com.XDApp.xdbase;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.XDApp.xdbase.bean.BaseEntity;


public class BaseObjectListAdapter extends BaseAdapter {

	protected BaseApplication mApplication;
	protected Context mContext;
	protected LayoutInflater mInflater;
	protected List<? extends BaseEntity> mDatas = new ArrayList<BaseEntity>();

	public BaseObjectListAdapter(BaseApplication application, Context context,
			List<? extends BaseEntity> datas) {
		mApplication = application;
		mContext = context;
		mInflater = LayoutInflater.from(context);
		if (datas != null) {
			mDatas = datas;
		}
	}
	@Override
	public int getCount() {
		return mDatas.size();
	}
	@Override
	public Object getItem(int position) {
		return mDatas.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return null;
	}
	public List<? extends BaseEntity> getDatas() {
		return mDatas;
	}
}
