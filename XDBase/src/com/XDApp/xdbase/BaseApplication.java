package com.XDApp.xdbase;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import cn.jpush.android.api.JPushInterface;

import com.XDApp.xdbase.activitys.MainActivity;
import com.XDApp.xdbase.bean.UpdateInfo;
import com.XDApp.xdbase.bean.User;
import com.XDApp.xdbase.xinsilu.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class BaseApplication extends Application {

	private Bitmap mDefaultAvatar;
	private static BaseApplication application;
	private SharedPreferences sp;
	/**
	 * 更新信息
	 */
	public UpdateInfo updateInfo;
	private User user;
	private boolean isLogin = false;

	private static final String AVATAR_DIR = "avatar/";
	public Map<String, SoftReference<Bitmap>> mAvatarCache;

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
		mAvatarCache = new HashMap<String, SoftReference<Bitmap>>();
		mDefaultAvatar = BitmapFactory.decodeResource(getResources(), R.drawable.default_avatar);

		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);

		sp = getSharedPreferences(MainActivity.class.getSimpleName(), MODE_PRIVATE);
		String userName = sp.getString("phone", "");
		String pwd = sp.getString("pwd", "");
		String nickName = sp.getString("nickName", "");
		String userLogo = sp.getString("userLogo", "");
		String userID = sp.getString("userID", "");
		if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pwd)) {
			user = new User();
			user.setName(userName);
			user.setPwd(pwd);
			user.setNickName(nickName);
			user.setUserLogo(userLogo);
			user.setUserID(userID);
			setUser(user);
			setLogin(true);
		} else {
			setLogin(false);
		}

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().resetViewBeforeLoading(true)
				.imageScaleType(ImageScaleType.EXACTLY).showImageForEmptyUri(R.drawable.ic_launcher)
				.bitmapConfig(Bitmap.Config.RGB_565).showImageOnFail(R.drawable.ic_launcher)
//				.considerExifParams(true)
//		.displayer(new FadeInBitmapDisplayer(300))
				.cacheInMemory(true).cacheOnDisc(true).build();

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.defaultDisplayImageOptions(defaultOptions).discCacheSize(100 * 1024 * 1024)//
				.discCacheFileCount(100)// 缓存一百张图片
				.build();

		ImageLoader.getInstance().init(config);

	}

	public static final BaseApplication getApplication() {
		return application;
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Log.e("BaseApplication", "onLowMemory");
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		Log.e("BaseApplication", "onTerminate");
	}

	public Bitmap getAvatar(String imageName) {
		if (mAvatarCache.containsKey(imageName)) {
			Reference<Bitmap> reference = mAvatarCache.get(imageName);
			if (reference.get() == null || reference.get().isRecycled()) {
				mAvatarCache.remove(imageName);
			} else {
				return reference.get();
			}
		}
		InputStream is = null;
		Bitmap bitmap = null;
		try {
			/*************************获取流**********************/
			is = getAssets().open(AVATAR_DIR + imageName);
			/*******************************************************/
			bitmap = BitmapFactory.decodeStream(is);
			if (bitmap == null) {
				throw new FileNotFoundException(imageName + "is not find");
			}
			mAvatarCache.put(imageName, new SoftReference<Bitmap>(bitmap));
			return bitmap;
		} catch (Exception e) {
			return mDefaultAvatar;
		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {

			}
		}
	}
}
