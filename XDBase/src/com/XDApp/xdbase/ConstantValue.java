package com.XDApp.xdbase;

public interface ConstantValue {
	//Jpush
	String NOTIFICATION_TITLE = "notificationTitle";
	String NOTIFICATION_CONTEXT = "notificationContext";
	
	//ecode
	String ENCODING = "UTF-8";
	String IMAGE_PATH = "";
	
	//splashTime
	long SPLASH_TIME = 500;
	
	//paperNum
	int GUIDECOUNT = 3;
}
