package com.XDApp.xdbase.utils;

public interface IRequest<T> {
	void request(T t);
}
