package com.XDApp.xdbase.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.bean.DetailPriceInfo;
import com.XDApp.xdbase.bean.ProductInfo;
import com.XDApp.xdbase.bean.TalkInfo;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class HttpProtocol {
	public static final String BASEURL = "http://www.silutrip.com/index.php?m=service&c=Account&a=";
	public static final String GETPRODUCTDETAIL = "GetProductDetail";
	public static final String ADDFAVORITE = "AddFavorite";
	public static final String CANCELFAVORITE = "CancelFavorite";
	public static final String ISFAVORITE = "IsFavorite";
	public static final String TYPE = "type";
	public static final String PRODUCTID = "productId";
	public static final String PAGENUM = "pageNum";
	public static final String PAGESIZE = "pageSize";
	public static final String SENERYID = "seneryId";
	public static final String USERID = "userID";
	public static final String FLAG = "flag";
	public static final String OSTYPE = "ostype";
	public static final String OSTYPENAME = "ANDROID";

	/**
	 * 产品类型
	 * @author chenghao
	 *
	 */
	public static enum PRODUCT_TYPE {
		/** 参团 */
		FREEWALKER(0), /** 自由行 */
		TRAVEL(1), /** 门票 */
		TICKET(2), /** 特产商城 */
		SPECIALTY(3);
		public int type;

		private PRODUCT_TYPE(int type) {
			this.type = type;
		}

	}

	public static void getProductDetailToPhotos(String productId, final IRequest<List<String>> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(0));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				iRequest.request(parserPhotos(content));
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(null);
			}
		});
	}

	public static List<String> parserPhotos(String content) {
		try {
			List<String> list = new ArrayList<String>();
			JSONArray ja = new JSONObject(content).getJSONObject("data").getJSONArray("productList");
			for (int i = 0; i < ja.length(); i++) {
				list.add(ja.getJSONObject(i).getString("url"));
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取商品的价格
	 * @param iRequest
	 */
	public static void getProductDetailForPrice(String productId, final IRequest<DetailPriceInfo> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(1));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseObject(new JSONObject(content).getJSONObject("data")
							.toString(), DetailPriceInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(null);
			}
		});
	}

	public static void getProductDetailToIntroduce(String productId, final IRequest<ProductInfo> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(2));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseObject(new JSONObject(content).getJSONObject("data")
							.toString(), ProductInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(null);
			}
		});
	}

	/**
	 * 获取详情评论
	 * @param iRequest
	 */
	public static void getProductDetailToComment(String productId, int pageNum, int pageSize,
			final IRequest<List<TalkInfo>> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(3));
		params.put(PRODUCTID, productId);
		params.put(PAGENUM, String.valueOf(pageNum));
		params.put(PAGESIZE, String.valueOf(pageSize));

		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseArray(new JSONObject(content).getJSONObject("data")
							.getJSONArray("talkList").toString(), TalkInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(null);
			}
		});
	}

	/**
	 * 添加收藏
	 */
	public static void addCollect(String seneryId, String userId, int flag, final IRequest<Boolean> iRequest) {
		RequestParams params = new RequestParams();
		params.put(SENERYID, seneryId);
		params.put(USERID, userId);
		params.put(OSTYPE, OSTYPENAME);
		params.put(FLAG, String.valueOf(flag));

		XDHttpClient.post(ADDFAVORITE, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				iRequest.request(true);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(false);
			}
		});
	}

	/**
	 * 取消收藏
	 */
	public static void cancelCollect(String seneryId, String userId, int flag,
			final IRequest<Boolean> iRequest) {
		RequestParams params = new RequestParams();
		params.put(SENERYID, seneryId);
		params.put(USERID, userId);
		params.put(OSTYPE, OSTYPENAME);
		params.put(FLAG, String.valueOf(flag));

		XDHttpClient.post(CANCELFAVORITE, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				iRequest.request(true);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(false);
			}
		});
	}

	/**
	 * 添加收藏
	 */
	public static void isCollect(String seneryId, String userId, int flag, final IRequest<Boolean> iRequest) {
		RequestParams params = new RequestParams();
		params.put(SENERYID, seneryId);
		params.put(USERID, userId);
		params.put(OSTYPE, OSTYPENAME);
		params.put(FLAG, String.valueOf(flag));

		XDHttpClient.post(ISFAVORITE, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				//					iRequest.request();
				try {
					if (1 == new JSONObject(content).getJSONObject("data").getInt("isFav")) {
						iRequest.request(true);
						return;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				iRequest.request(false);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
				iRequest.request(false);
			}
		});
	}
}
