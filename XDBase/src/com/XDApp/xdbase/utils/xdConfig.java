package com.XDApp.xdbase.utils;

public class xdConfig {

	/**
	 * 当前版本号:1.0
	 */
	public static final String VERSION = "1.0";

	public final static String HOST_URL = "http://www.silutrip.com/index.php?m=service&c=Account&a=";
	public final static int MAX_LENGTH = 96;
	public final static String ANDRODID = "ANDROID";// 安卓端
	public final static int LIMIT = 10;

	public final static String LOGIN = "Login";// 登录
	public final static String LOGOUT = "Logout";// 注销
	public final static String CHANGEPWD = "Updatepassword";// 修改密码
	public final static String CHANGE_NICKNAME = "UpdateuserName";// 修改昵称
	public final static String CHANGE_HEADICON = "Upimage";// 修改头像
	public final static String GETVERIFYCODE = "Getverifycode";// 获得验证码
	public final static String CHECKVERIFYCODE = "Checkverifycode";// 验证验证码
	public final static String MOBILEREGISTER = "Mobileregister";// 注册
	public final static String HOME = "Home";// 获取首页数据
	public final static String GET_FREETOURLIST = "getFreetourList";// 获取自由行和参团游列表
	public final static String GET_FREETOURDETAIL = "getFreetourDetail";// 自由行和参团游详情
	public final static String GET_TICKETLIST = "GetTicketList";// 门票列表
	public final static String GET_PRODUCTLIST = "GetProductList";// 特产商城列表
	public final static String GET_MYCOMMENTLIST = "GetMyCommentList";// 我的点评列表
	/** 预订门票*/
	public final static String GET_RESERVETICKET = "ReserveTicket";
	/** 预订特产*/
	public final static String GET_RESERVESPECIAL = "ReserveSpecial";
	/** 订单详情*/
	public final static String GET_GETORDERDETAIL = "GetOrderDetail";
	public final static String RESETPASSWORD = "Resetpassword";
	/** 常用联系人获取*/
	public final static String GET_MYCONTACT = "GetMyContact";
	/** 修改常用联系人*/
	public final static String UPDATE_CONTACT = "UpdateContact";
	/** 常用地址获取*/
	public final static String GET_MYSENDADDRESS = "getMySendAddress";
	/** 修改常用地址*/
	public final static String UPDATE_SENDERADDRESS = "UpdateSenderAddress";

	/** 取消订单*/
	public final static String GET_CANORDER = "CancleOrder";

	/** 支付成功*/
	public final static String SEND_ORDER = "Sendorder";
	/** 支付成功*/
	public final static String UPDATE_SEX = "UpdateSex";

}
