package com.XDApp.xdbase.utils;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.bean.DetailPriceInfo;
import com.XDApp.xdbase.bean.ProductInfo;
import com.XDApp.xdbase.bean.TalkInfo;
import com.XDApp.xdbase.bean.TicketInfo;
import com.XDApp.xdbase.bean.TicketPriceInfo;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class TicketHttpProtocol {
	public static final String BASEURL = "http://www.silutrip.com/index.php?m=service&c=Account&a=";
	public static final String GETPRODUCTDETAIL = "GetTicketDetail";
	public static final String TYPE = "type";
	public static final String PRODUCTID = "seneryId";
	public static final String PAGENUM = "pageNum";
	public static final String PAGESIZE = "pageSize";

	public static void getProductDetailToPhotos(String productId, final IRequest<List<String>> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(0));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				iRequest.request(parserPhotos(content));
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
			}
		});
	}

	public static List<String> parserPhotos(String content) {
		try {
			List<String> list = new ArrayList<String>();
			JSONArray ja = new JSONObject(content).getJSONObject("data").getJSONArray("seneryList");
			for (int i = 0; i < ja.length(); i++) {
				list.add(ja.getJSONObject(i).getString("url"));
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取商品的价格
	 * @param iRequest
	 */
	public static void getProductDetailForPrice(String productId, final IRequest<TicketPriceInfo> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(1));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseObject(new JSONObject(content).getJSONObject("data")
							.toString(), TicketPriceInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
			}
		});
	}

	public static void getProductDetailToIntroduce(String productId, final IRequest<TicketInfo> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(2));
		params.put(PRODUCTID, productId);
		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseObject(new JSONObject(content).getJSONObject("data")
							.toString(), TicketInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
			}
		});
	}

	/**
	 * 获取详情评论
	 * @param iRequest
	 */
	public static void getProductDetailToComment(String productId, int pageNum, int pageSize,
			final IRequest<List<TalkInfo>> iRequest) {
		RequestParams params = new RequestParams();
		params.put(TYPE, String.valueOf(3));
		params.put(PRODUCTID, productId);
		params.put(PAGENUM, String.valueOf(pageNum));
		params.put(PAGESIZE, String.valueOf(pageSize));

		XDHttpClient.post(GETPRODUCTDETAIL, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(String content) {
				super.onSuccess(content);
				try {
					iRequest.request(JSON.parseArray(new JSONObject(content).getJSONObject("data")
							.getJSONArray("talkList").toString(), TalkInfo.class));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);
			}
		});
	}
}
