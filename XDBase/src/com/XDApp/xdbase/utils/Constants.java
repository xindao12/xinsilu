package com.XDApp.xdbase.utils;

public class Constants {

	public final static String HOST_URL = "http://www.silutrip.com/index.php?m=service&c=Account&a=";
	public final static int PAGE_SIZE = 10;

	public final static String ORDER_SENERY = "GetSeneryOrderList";// 景点门票订单
	public final static String ORDER_WALKER = "GetWalkerOrderList";// 自由行订单
	public final static String ORDER_GROUP_PLAY = "GetSeneryOrderList";// 参团游订单
	public final static String ORDER_SPECIAL = "GetSpecialOrderList";// 特产订单
	public final static String STORE_INFOR = "GetMyFavoriteList/";// 收藏信息

}
