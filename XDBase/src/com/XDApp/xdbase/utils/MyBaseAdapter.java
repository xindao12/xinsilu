package com.XDApp.xdbase.utils;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class MyBaseAdapter<T> extends BaseAdapter {
	private List<T> mList;
	private Context mContext;
	private int resId;

	public MyBaseAdapter(Context context, List<T> mList, int resId) {
		super();
		mContext = context;
		this.mList = mList;
		this.resId = resId;
	}

	public void reset(List<T> list) {
		this.mList = list;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	public List<T> getDatas() {
		return mList;
	}

	@Override
	public T getItem(int arg0) {
		return mList.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int position, View contentView, ViewGroup arg2) {
		if (contentView == null) {
			contentView = View.inflate(mContext, resId, null);
		}
		return getView(position, contentView, arg2, getItem(position));
	}

	public abstract View getView(int position, View contentView,
			ViewGroup parent, T t);

}
