package com.XDApp.xdbase.utils;

import java.io.ByteArrayOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.PaintDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupWindow;

import com.XDApp.xdbase.xinsilu.R;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXWebpageObject;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.UMWXHandler;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.media.UMImage;

/**
 * 分享
 * @author Administrator
 *
 */
public class SharePopupWindow extends PopupWindow implements OnClickListener {
//	private RelativeLayout rlShareToSina;
//	private RelativeLayout rlShareToWeixin;
	private Button bt_share_weixin;
	private Button bt_share_weibo;
	private Button bt_share_cancle;
//	private RelativeLayout rlShareToFriends;
//	private RelativeLayout rlShareToSms;
	private UMSocialService mController;
	private String shareContent;
	private String shareTitle;
	private String urlContent;
	private Context mContext;
	String wxAppID = "wxa7498a72fd4e90b7";

	public SharePopupWindow(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(context);
	}

	public void setInfors(UMSocialService mController, String shareTitle, String shareContent,
			String urlContent) {
		this.mController = mController;
		this.shareTitle = shareTitle;
		this.shareContent = shareContent;
		this.urlContent = urlContent;
	}

	public void sendReq(Context context, String text, Bitmap bmp, int type) {
		IWXAPI api = WXAPIFactory.createWXAPI(context, wxAppID, true);
		if (!api.isWXAppInstalled()) {
			// UIHelper.showToast(context, "您没有安装微信，请下载后分享...");
			return;
		}

		String url = urlContent;// 收到分享的好友点击信息会跳转到这个地址去
		WXWebpageObject localWXWebpageObject = new WXWebpageObject();
		localWXWebpageObject.webpageUrl = url;
		WXMediaMessage localWXMediaMessage = new WXMediaMessage(localWXWebpageObject);
		localWXMediaMessage.title = "兰格钢铁";// 不能太长，否则微信会提示出错。不过博主没验证过具体能输入多长。
		localWXMediaMessage.description = text;
		localWXMediaMessage.thumbData = getBitmapBytes(bmp, false);
		SendMessageToWX.Req localReq = new SendMessageToWX.Req();
		localReq.transaction = System.currentTimeMillis() + "";
		localReq.message = localWXMediaMessage;
		localReq.scene = type;
		api.sendReq(localReq);
	}

	// 需要对图片进行处理，否则微信会在log中输出thumbData检查错误
	private static byte[] getBitmapBytes(Bitmap bitmap, boolean paramBoolean) {
		Bitmap localBitmap = Bitmap.createBitmap(80, 80, Bitmap.Config.RGB_565);
		Canvas localCanvas = new Canvas(localBitmap);
		int i;
		int j;
		if (bitmap.getHeight() > bitmap.getWidth()) {
			i = bitmap.getWidth();
			j = bitmap.getWidth();
		} else {
			i = bitmap.getHeight();
			j = bitmap.getHeight();
		}
		while (true) {
			localCanvas.drawBitmap(bitmap, new Rect(0, 0, i, j), new Rect(0, 0, 80, 80), null);
			if (paramBoolean)
				bitmap.recycle();
			ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
			localBitmap.compress(Bitmap.CompressFormat.JPEG, 100, localByteArrayOutputStream);
			localBitmap.recycle();
			byte[] arrayOfByte = localByteArrayOutputStream.toByteArray();
			try {
				localByteArrayOutputStream.close();
				return arrayOfByte;
			} catch (Exception e) {

			}
			i = bitmap.getHeight();
			j = bitmap.getHeight();
		}
	}

	private void init(Context context) {
		mContext = context;
		View convertView = View.inflate(context, R.layout.popup, null);
//		rlShareToFriends = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_friends);
//		rlShareToWeixin = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_weixin);
//		rlShareToSina = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_sina);
		bt_share_weixin = (Button) convertView.findViewById(R.id.bt_share_weixin);
		bt_share_weibo = (Button) convertView.findViewById(R.id.bt_share_weibo);
		bt_share_cancle = (Button) convertView.findViewById(R.id.bt_share_weibo);
//		rlShareToSms = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_sms);
//		rlShareToFriends.setOnClickListener(this);
		bt_share_weixin.setOnClickListener(this);
		bt_share_weibo.setOnClickListener(this);
		bt_share_cancle.setOnClickListener(this);
//		rlShareToSms.setOnClickListener(this);
		setBackgroundDrawable(new PaintDrawable(Color.TRANSPARENT));
		setOutsideTouchable(true);
	}

	public SharePopupWindow(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);

	}

	public SharePopupWindow(Context context) {
		super(context);
		init(context);
	}

	public SharePopupWindow(Context context, View convertView, int width, int height) {
		super(convertView, width, height);
		init(context, convertView);
	}

	private void init(Context context, View convertView) {
		mContext = context;
//		rlShareToFriends = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_friends);
//		rlShareToWeixin = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_weixin);
//		rlShareToSina = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_sina);
		bt_share_weixin = (Button) convertView.findViewById(R.id.bt_share_weixin);
		bt_share_weibo = (Button) convertView.findViewById(R.id.bt_share_weibo);
		bt_share_cancle = (Button) convertView.findViewById(R.id.bt_share_cancle);
//		rlShareToSms = (RelativeLayout) convertView.findViewById(R.id.rl_share_to_sms);
//		rlShareToFriends.setOnClickListener(this);
		bt_share_weixin.setOnClickListener(this);
		bt_share_weibo.setOnClickListener(this);
		bt_share_cancle.setOnClickListener(this);
//		rlShareToSms.setOnClickListener(this);
		setBackgroundDrawable(new PaintDrawable(Color.TRANSPARENT));
		setOutsideTouchable(true);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
//		case R.id.rl_share_to_friends:// 朋友圈
//			if (isShowing()) {
//				dismiss();
//			}
//
//			// 支持微信朋友圈
//			UMWXHandler umWXHandler = mController.getConfig().supportWXCirclePlatform(mContext, wxAppID,
//					urlContent);
//			umWXHandler.setCircleTitle(shareTitle);
//			mController.setShareImage(new UMImage(mContext, R.drawable.share_icon));
//			mController.setShareContent(shareContent);
//			mController.postShare(mContext, SHARE_MEDIA.WEIXIN_CIRCLE, new SnsPostListener() {
//				@Override
//				public void onComplete(SHARE_MEDIA arg0, int arg1, SocializeEntity arg2) {
//				}
//
//				@Override
//				public void onStart() {
//
//				}
//			});
////			sendReq(mContext,
////					shareContent+urlContent,
//////					content,
//////					BitmapFactory.decodeFile(imagePath),
////					BitmapFactory.decodeResource(
////							mContext.getResources(),
////							R.drawable.icon_share),
////					Req.WXSceneSession);
//
//			break;
		case R.id.bt_share_weibo:// 新浪微博
			if (isShowing()) {
				dismiss();
			}
			mController.setShareContent(shareContent + urlContent);
			mController.setShareImage(new UMImage(mContext, R.drawable.share_icon));
			mController.postShare(mContext, SHARE_MEDIA.SINA, new SnsPostListener() {
				@Override
				public void onComplete(SHARE_MEDIA arg0, int arg1, SocializeEntity arg2) {
				}

				@Override
				public void onStart() {

				}
			});
			break;
		case R.id.bt_share_weixin:// 微信
			if (isShowing()) {
				dismiss();
			}
			// 支持微信好友分享
			UMWXHandler wxHandler = mController.getConfig().supportWXPlatform(mContext, wxAppID, urlContent);
			wxHandler.setWXTitle(shareTitle);
			mController.setShareImage(new UMImage(mContext, R.drawable.share_icon));
			mController.setShareContent(shareContent);
			mController.postShare(mContext, SHARE_MEDIA.WEIXIN, new SnsPostListener() {
				@Override
				public void onComplete(SHARE_MEDIA arg0, int arg1, SocializeEntity arg2) {
				}

				@Override
				public void onStart() {

				}
			});
			break;
//		case R.id.rl_share_to_sms:// 短信
//			if (isShowing()) {
//				dismiss();
//			}
//			Intent smsintent = sendSMS("", shareContent + urlContent);
//			mContext.startActivity(smsintent);
//			break;
		case R.id.bt_share_cancle:
			if (isShowing()) {
				dismiss();
			}

			break;
		default:
			break;
		}
	}

//	public static Intent sendSMS(String phone, String msg) {
//		Intent intent = new Intent(Intent.ACTION_SENDTO);
//		intent.putExtra("sms_body", msg);
//		intent.setData(Uri.parse("smsto:" + toSafeString(phone)));
//		return intent;
//	}

	private static String toSafeString(Object obj) {
		if (null == obj) {
			return "";
		}
		return obj.toString();
	}
}
