package com.XDApp.xdbase.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;

public class UpdateUserState {
	static List<IUpdateUserState> listeners = new ArrayList<IUpdateUserState>();

	public static void add(IUpdateUserState listener) {
		listeners.add(listener);
	}

	public static void remove(IUpdateUserState listener) {
		listeners.remove(listener);
	}

	public static void notifyUpdateUser(Intent data) {
		for (IUpdateUserState listener : listeners) {
			listener.update(data);
		}
	}

	public interface IUpdateUserState {
		void update(Intent data);
	}
}
