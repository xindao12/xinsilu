package com.XDApp.xdbase.utils;

public class xdConstant {
	public static String DATABASE_NAME = "database";

	// 提示
	public final static String tip_service_error_msg = "服务器错误:";
	public final static String tip_network_error = "网络不给力，请检查网络";
	public final static String tip_loading = "正在加载...";
	public final static String PLATFORM_ANDROID = "android";

	public final static String key_version = "version";
	public final static String key_platform = "platform";
	public final static String intent_aid = "intent_aid";
	public final static String key_uid_local = "key_userid"; // app的用户uid
	public final static String key_token_local = "key_token"; // 保持登录的token
	public final static String key_token = "token";
	public final static String key_deviceid = "sn";
	// 快讯
	public final static String HOT = "hot"; // 热点
	public final static String FINANCE = "finance";// 财经
	public final static String VIEWPOINT = "viewpoint";//视点 
	public final static String INDUSTRY = "industry";// 产经
	//期货
	public final static String ANALY = "analy"; // 分析
	public final static String NOTE = "note";// 手记
	public final static String ORG = "org";//机构
	public final static String OUTSIDE = "outside";// 外盘
	// 订阅中心
	public static final int HANG_QING = 1;// 行情订阅
	public static final int HANG_QING_MY_ORDER = 11;//行情--我的订阅
	public static final int HANG_QING_SHOW_DETAIL = 21;  //行情详情页
	public static final int HANG_QING_FROM_SUBSCRIE = 31;  //行情的订阅按钮
	public static final int HANG_QING_MENU = 41;
	public static final int HANG_QING_DETAIL_CONTENT =51;//行情详细页面判断是否为多用户登录
	//------
	public static final int KUAI_XUN = 2;// 快讯订阅
	public static final int KUAI_XUN_MY_ORDER = 12;// 快讯 --我的订阅
	public static final int KUAI_XUN_SHOW_DETAIL = 22;  //快讯详情页
	public static final int KUAI_XUN_FROM_SUBSCRIE = 32;//快讯的订阅按钮
	//----------
	public static final int GANG_CHANG = 3;// 钢厂订阅
	public static final int GAGNG_CHANGE_MY_ORDER = 13;//钢厂 ---我的订阅
	public static final int GANG_CHANG_SHOW_DETAIL = 23;  //钢厂详情页
	public static final int GANG_CHAGN_FROM_SUBSCRIE = 33;//钢厂的订阅按钮
	public static final int GANG_CHANG_MENU = 43;//钢厂的菜单键
	public static final int GANG_CHANG_DETAIL_CONTENT = 53;
	
	public static final int QI_HUO_SHOW_DETAIL = 7;//期货详情界面
	public static final int ZI_XUN_SHOW_DETAIL = 8;//资讯详情界面
	
	public static final int LOGIN_SUCCESS = 4;
	public static final int LOGIN_FAIL = 5;
	public static final int CHANGE_PWD = 6;
	
	public static final int KUAI_XUN_FROM_NINE = 40;
	public static final int HANG_QING_FROM_NINE = 41;
	public static final int GANG_CHANG_FROM_NINE = 42;
	public static final int QI_HUO_FROM_NINE = 43;
	
	public static final int SUBSCRIBE_CENTER = 50;
	public final static int WAIT_LENGTH_LONG = 1200;
	


}
