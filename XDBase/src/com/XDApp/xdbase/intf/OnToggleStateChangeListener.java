package com.XDApp.xdbase.intf;

public interface OnToggleStateChangeListener {

	/**
	 * 当开关状态改变时回调
	 * @param state 当前开关的状态
	 */
	public void onToggleStateChanged(boolean state);
}
