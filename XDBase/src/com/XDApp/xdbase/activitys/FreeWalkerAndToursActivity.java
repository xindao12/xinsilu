package com.XDApp.xdbase.activitys;

import java.util.List;

import org.json.JSONException;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.NoContentAdapter;
import com.XDApp.xdbase.bean.FreeWalker;
import com.XDApp.xdbase.bean.FreeWalkerEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 自由行和参团游列表
 * 
 * @author Administrator
 * 
 */
public class FreeWalkerAndToursActivity extends BaseActivity implements OnClickListener {
	private static final String TAG = "FreeWalkerActivity";
	private TextView title_text;
	private ImageView iv_back;
	private PullToRefreshListView listView;
	private NoContentAdapter adapter;

	private String flag;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_freewalker_and_tours);
		flag = getIntent().getStringExtra("flag");
		title_text = (TextView) findViewById(R.id.title_text);
		iv_back = (ImageView) findViewById(R.id.title_back);
		listView = (PullToRefreshListView) findViewById(R.id.listView);
		iv_back.setOnClickListener(this);
		initData();
		refreshAndLoadMore();
	}

	private void initData() {
		getData(mContext, flag, 1, xdConfig.LIMIT, new ILoadNextPageData<FreeWalker>() {

			@Override
			public void loadNextPageData(List<FreeWalker> t) {
				if (null != t) {
					adapter = new NoContentAdapter(mContext, t, xdConfig.LIMIT,
							R.layout.activity_freewalker_item, R.layout.item_loading, flag);
					listView.setAdapter(adapter);
//					listView.onLoadSuccess();
					listView.setPullLabel("加载成功");
				} else {
					listView.setPullLabel("加载失败");
//					listView.onLoadFailed();
					CRToast.show(mContext, "获取数据失败");
				}
				listView.onRefreshComplete();
			}
		});
	}

	@Override
	protected void initEvents() {
		/*listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Intent intent=new Intent(FreeWalkerAndToursActivity.this,FreeWalkerDetailActivity.class);
				intent.putExtra("seneryID", list.get(position).getSeneryId());
				intent.putExtra("flag", flag);
				startActivity(intent);
			}
		});*/

		if ("0".equals(flag)) {
			title_text.setText("自由行");
		} else if ("1".equals(flag)) {
			title_text.setText("参团游");
		}
	}

	private void refreshAndLoadMore() {
		listView.setMode(Mode.PULL_FROM_START);
		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				initData();
			}
		});
	}

	public static void getData(final Context mContext, String flag, int pageNumber, int size,
			final ILoadNextPageData<FreeWalker> iRequest) {
//		if (CommonUtil.checkNetState(mContext)) {
		RequestParams params = new RequestParams();
		params.put("flag", flag);
		params.put("pageNum", String.valueOf(pageNumber));
		params.put("pageSize", String.valueOf(size));

		XDHttpClient.post(xdConfig.GET_FREETOURLIST, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, String content) {
				// VolleyLog.e("freeWalk:%s", content);
				org.json.JSONObject obj;
				String data = null;
				try {
					obj = new org.json.JSONObject(content);
					data = obj.getString("data");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				if (TextUtils.isEmpty(data)) {
					CommonUtil.showToast(mContext, "获取数据失败");
					return;
				}

				FreeWalkerEntity parseObject = JSON.parseObject(data, FreeWalkerEntity.class);
				Log.i(TAG, data);

				// 判断返回数据是否正确
				if (null != parseObject) {
					iRequest.loadNextPageData(parseObject.getList());
					// 关闭掉这个Activity
				} else {
					iRequest.loadNextPageData(null);
					// CommonUtil.showToast(mContext, "获取数据失败");
				}
			}

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				super.onFailure(arg0, arg1);
				iRequest.loadNextPageData(null);
				// Toast.makeText(mContext, R.string.pLease_check_network,
				// 0).show();
			}
		});
//		} else {
//			iRequest.loadNextPageData(null);
		// Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
//		}
	}

	// TODO 访问网络获取数据
	/*private void getData(int pageNumber, final boolean RELEASE_TO_REFRESH) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("flag", flag);
			params.put("pageNum", String.valueOf(pageNumber));
			params.put("pageSize", String.valueOf(xdConfig.LIMIT));

	//			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(xdConfig.GET_FREETOURLIST, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					Log.e(TAG, content);
	//					if (null != ProgressDialog && ProgressDialog.isShowing())
	//						ProgressDialog.dismiss();

					JSONObject obj = new JSONObject().parseObject(content);
					String data = obj.getString("data");
					if (TextUtils.isEmpty(data)) {
						CommonUtil.showToast(mContext, "获取数据失败");
						return;
					}

					FreeWalkerEntity parseObject = JSON.parseObject(data, FreeWalkerEntity.class);
					Log.i(TAG, data);

					// 判断返回数据是否正确
					if (null != parseObject) {
						list = parseObject.getList();
						totalCount=parseObject.getTotalCount();
						resetAdapter(list);
						if (RELEASE_TO_REFRESH) {
							listView.setState(PullToRefreshListView.DONE);
							listView.changeHeaderViewByState();
						} else {
							listView.onRefreshFinish();
						}
						// 关闭掉这个Activity
					} else {
						CommonUtil.showToast(mContext, "获取数据失败");
					}
				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
	//					if (null != ProgressDialog && ProgressDialog.isShowing()) {
	//						ProgressDialog.dismiss();
	//					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
					if (RELEASE_TO_REFRESH) {
						listView.setState(PullToRefreshListView.DONE);
						listView.changeHeaderViewByState();
					} else {
						listView.onRefreshFinish();
					}
				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}*/

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}
}
