package com.XDApp.xdbase.activitys;

import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.utils.UpdateUserState;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ChangePwdActivity extends BaseActivity implements OnClickListener {
	private final String TAG = "ChangePwdActivity";
	private ImageView iv_back;
	private TextView tv_title, tv_right;
	private EditText et_oldPwd, et_newPwd, et_againPwd;
	private String old;
	private String newPwd;
	private String againPwd;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_changepwd);

		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);
		tv_right = (TextView) findViewById(R.id.title_right);
		et_oldPwd = (EditText) findViewById(R.id.oldpwd);
		et_newPwd = (EditText) findViewById(R.id.newpwd);
		et_againPwd = (EditText) findViewById(R.id.againpwd);

		iv_back.setOnClickListener(this);
		tv_right.setOnClickListener(this);
	}

	@Override
	protected void initEvents() {
		tv_right.setText("确定");
		tv_right.setTextColor(Color.WHITE);
		tv_title.setText("修改密码");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_back:
			finish();
			break;
		case R.id.title_right:// 确定按钮
			old = et_oldPwd.getText().toString().trim();
			newPwd = et_newPwd.getText().toString().trim();
			againPwd = et_againPwd.getText().toString().trim();
			if (TextUtils.isEmpty(old) || TextUtils.isEmpty(newPwd) || TextUtils.isEmpty(againPwd)) {
				CRToast.show(ChangePwdActivity.this, "密码不能为空");
				return;
			}
			if (!newPwd.equals(againPwd)) {
				CRToast.show(ChangePwdActivity.this, "两次密码输入不一致");
				return;
			}
			if (!(newPwd.length() > 5 && newPwd.length() < 17)) {
				CRToast.show(ChangePwdActivity.this, "请输入6--16位之间的密码");
				return;
			}
			getData(old, newPwd, getIntent().getStringExtra("userID"));

			break;
		default:
			break;
		}
	}

	// TODO 访问网络获取数据
	private void getData(String oldPwd, String newPwd, String name) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("oldPassword", LoginActivity.md5(oldPwd));
			params.put("password", LoginActivity.md5(newPwd));
			params.put("userID", name);
			params.put("ostype", xdConfig.ANDRODID);

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(xdConfig.CHANGEPWD, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();
					Log.i(TAG, content);

					JSONObject obj;
					String data = null;
					try {
						// 修改成功
						if (new JSONObject(content).getInt("state") == 1) {
							CommonUtil.showToast(mContext, "密码修改成功！");

							// 重新登录
							BaseApplication.getApplication().setLogin(false);
							Intent intent = new Intent(getBaseContext(), LoginActivity.class);
							startActivity(intent);

							UpdateUserState.notifyUpdateUser(null);

							finish();
						} else {
							CommonUtil.showToast(mContext, "密码修改失败，请检查原密码是否正确！");
						}

						/*obj = new JSONObject(content);
						data = obj.getString("data");
						FreeWalkerEntity parseObject = JSON.parseObject(data, FreeWalkerEntity.class);

						// 判断返回数据是否正确
						if (null != parseObject) {
							List<FreeWalker> list = parseObject.getList();
							// 关闭掉这个Activity
						} else {
							CommonUtil.showToast(mContext, "获取数据失败");
						}*/
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();

				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}
}
