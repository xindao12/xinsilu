package com.XDApp.xdbase.activitys;

import android.webkit.WebView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.xinsilu.R;

public class PayWebActivity extends BaseActivity {
	private WebView wv_pay;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_pay_web);
		wv_pay = (WebView) findViewById(R.id.wv_pay);

	}

	@Override
	protected void initEvents() {
		wv_pay.loadUrl("http://121.101.223.103:443/pay/alipayapi.jsp");
	}

}
