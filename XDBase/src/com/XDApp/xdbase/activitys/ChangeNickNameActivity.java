package com.XDApp.xdbase.activitys;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ChangeNickNameActivity extends BaseActivity implements OnClickListener {
	private final String TAG = "ChangeNickNameActivity";
	private ImageView iv_back;
	private TextView tv_title, tv_right;
	private EditText et_username;
	private SharedPreferences sp;
	private String userID;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_changenickname);
		sp = getSharedPreferences(MainActivity.class.getSimpleName(), MODE_PRIVATE);
		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);
		tv_right = (TextView) findViewById(R.id.title_right);
		et_username = (EditText) findViewById(R.id.username);

		iv_back.setOnClickListener(this);
		tv_right.setOnClickListener(this);

		userID = getIntent().getStringExtra("userID");
	}

	@Override
	protected void initEvents() {
		tv_right.setText("确定");
		tv_right.setTextColor(Color.WHITE);
		tv_title.setText("修改昵称");
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_back:
			finish();
			break;
		case R.id.title_right:// 确定按钮
			String username = et_username.getText().toString().trim();
			if (!TextUtils.isEmpty(username)) {
				getData(username);
			} else {
				CRToast.show(ChangeNickNameActivity.this, "用户名不能为空");
				return;
			}
			break;
		default:
			break;
		}
	}

	// TODO 访问网络获取数据
	private void getData(final String userName) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("userName", userName);// userID
			params.put("userID", userID);
			params.put("ostype", xdConfig.ANDRODID);

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(xdConfig.CHANGE_NICKNAME, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					Log.e(TAG, content);
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();

					JSONObject obj;
					String state = null;
					try {
						obj = new JSONObject(content);
						state = obj.getString("state");
					} catch (JSONException e) {
						e.printStackTrace();
					}

					if ("1".equals(state)) {
						CRToast.show(ChangeNickNameActivity.this, "修改成功");
						Editor editor = sp.edit();
						editor.putString("nickName", userName).commit();
						Intent data = new Intent();
						data.putExtra("nickName", userName);
						setResult(20001, data);
						finish();
					} else {
						CRToast.show(ChangeNickNameActivity.this, "修改失败,请重新修改");
						return;
					}
				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}
}
