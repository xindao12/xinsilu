package com.XDApp.xdbase.activitys;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.TicketAdapter;
import com.XDApp.xdbase.bean.Ticket;
import com.XDApp.xdbase.bean.TicketEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 门票列表
 * @author Administrator
 *
 */
public class TicketActivity extends BaseActivity implements OnClickListener {
	private TextView title_text;
	private ImageView iv_back;
	private PullToRefreshListView listView;
	private TicketAdapter adapter;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_ticket);
		title_text = (TextView) findViewById(R.id.title_text);
		iv_back = (ImageView) findViewById(R.id.title_back);
		listView = (PullToRefreshListView) findViewById(R.id.listView);
		iv_back.setOnClickListener(this);

		title_text.setText("门票");
		initData();
		refreshAndLoadMore();
	}

	private void initData() {
		getData(mContext, 0, xdConfig.LIMIT, new ILoadNextPageData<Ticket>() {

			@Override
			public void loadNextPageData(List<Ticket> t) {
				if (null != t) {
					adapter = new TicketAdapter(mContext, t, xdConfig.LIMIT, R.layout.activity_ticket_item,
							R.layout.item_loading);
					listView.setAdapter(adapter);
					listView.setPullLabel("加载成功");
//					listView.onLoadSuccess();
				} else {
					listView.setPullLabel("加载失败");
//					listView.onLoadFailed();
					CRToast.show(mContext, "获取数据失败");
				}
				listView.onRefreshComplete();
			}
		});
	}

	private void refreshAndLoadMore() {
		listView.setMode(Mode.PULL_FROM_START);
		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				initData();
			}
		});
	}

	// TODO 访问网络获取数据
	public static void getData(final Context mContext, int pageNumber, int pageSize,
			final ILoadNextPageData<Ticket> iRequest) {
//		if (CommonUtil.checkNetState(mContext)) {
		RequestParams params = new RequestParams();
		params.put("pageNum", String.valueOf(pageNumber));
		params.put("pageSize", String.valueOf(xdConfig.LIMIT));

		XDHttpClient.post(xdConfig.GET_TICKETLIST, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, String content) {
				JSONObject obj = new JSONObject().parseObject(content);
				String data = obj.getString("data");
				if (TextUtils.isEmpty(data)) {
					CommonUtil.showToast(mContext, "获取数据失败");
					return;
				}

				TicketEntity parseObject = JSON.parseObject(data, TicketEntity.class);
				// Log.i(TAG, data);

				// 判断返回数据是否正确
				if (null != parseObject) {
					iRequest.loadNextPageData(parseObject.getTicketList());
					// 关闭掉这个Activity
				} else {
					iRequest.loadNextPageData(null);
					// CommonUtil.showToast(mContext, "获取数据失败");
				}
			}

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				iRequest.loadNextPageData(null);
			}
		});
//		} else {
//			iRequest.loadNextPageData(null);
		// Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
//		}
	}

	@Override
	protected void initEvents() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// Intent intent=new
				// Intent(TicketActivity.this,FreeWalkerDetailActivity.class);
				// intent.putExtra("ticketID",
				// list.get(position).getTicketId());
				// startActivity(intent);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}
}
