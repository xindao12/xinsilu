package com.XDApp.xdbase.activitys;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.MyCommentAdapter;
import com.XDApp.xdbase.bean.CommentList;
import com.XDApp.xdbase.bean.CommentListEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Constants;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * @author admin 我的点评
 * 
 */
public class MyCommentActivity extends BaseActivity implements OnClickListener {
	private PullToRefreshListView listview;
	private TextView title;
	private TextView tvNoComplete;
	private TextView tvHasComplete;
	private static int type = 0;// 默认为未点评，1为已点评
	private ImageView imageView;
	private final View[] mTabViews = new View[2];

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_mycomment);
		listview = (PullToRefreshListView) findViewById(R.id.listview_comment_list);
		tvNoComplete = (TextView) findViewById(R.id.tv_no_complete);
		tvNoComplete.setOnClickListener(this);
		tvHasComplete = (TextView) findViewById(R.id.tv_has_complete);
		tvHasComplete.setOnClickListener(this);
		mTabViews[0] = tvNoComplete;
		mTabViews[1] = tvHasComplete;
		title = (TextView) findViewById(R.id.title_text);
		imageView = (ImageView) findViewById(R.id.imageview);
		title.setText("我的点评");
		listview.setMode(Mode.PULL_FROM_START);
		listview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				initDatas();
			}
		});
		initDatas();
		setTabSelected(0);
	}

	private void setTabSelected(int index) {
		for (int i = 0; i < mTabViews.length; i++) {
			if (index == i) {
				mTabViews[i].setSelected(true);
			} else {
				mTabViews[i].setSelected(false);
			}
		}
	}

	public static void initData(Context mContext, int pageNum, int pageSize,
			final ILoadNextPageData<CommentList> iLoadNextPageData) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("favType", "" + type);
			params.put("pageNum", "" + pageNum);
			params.put("pageSize", "" + Constants.PAGE_SIZE);
			params.put("userID", "" + BaseApplication.getApplication().getUser().getUserID());
			params.put("ostype", xdConfig.ANDRODID);

			XDHttpClient.post(xdConfig.GET_MYCOMMENTLIST, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (TextUtils.isEmpty(content)) {
						return;
					}
					CommentListEntity entity;
					try {
						entity = JSON.parseObject(new JSONObject(content).getJSONObject("data").toString(),
								CommentListEntity.class);
						if (1 == (new JSONObject(content).getInt("state"))) {// 成功
							iLoadNextPageData.loadNextPageData(entity.getList());
						} else {
							iLoadNextPageData.loadNextPageData(null);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(Throwable error, String content) {
					iLoadNextPageData.loadNextPageData(null);
				}

			});
		} else {// 提示网络未连接
		}

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.tv_no_complete:
			type = 0;
			initDatas();
			setTabSelected(0);
			break;
		case R.id.tv_has_complete:
			type = 1;
			initDatas();
			setTabSelected(1);
			break;
		case R.id.title_back:
			finish();
			break;
		}
	}

	private void initDatas() {
		initData(mContext, 1, xdConfig.LIMIT, new ILoadNextPageData<CommentList>() {

			@Override
			public void loadNextPageData(List<CommentList> t) {
				if (null != t) {
					listview.setAdapter(new MyCommentAdapter(type, mContext, t, xdConfig.LIMIT,
							R.layout.activity_mycomment_item, R.layout.item_loading));
					listview.setPullLabel("加载成功");
				} else {
					listview.setPullLabel("加载失败");
					CommonUtil.showToast(mContext, "获取数据失败！");
				}
				listview.onRefreshComplete();
			}
		});
	}

	@Override
	protected void initEvents() {
	}

}
