package com.XDApp.xdbase.activitys;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.UpdateService;
import com.XDApp.xdbase.view.CRDialog;
import com.XDApp.xdbase.view.CRDialog.OnCheckedListener;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;

public class AboutActivity extends BaseActivity implements OnClickListener {
	private ImageView iv_back;
	private TextView tv_title, tv_version;
	private Button bt_update;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_about);
		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);
		bt_update = (Button) findViewById(R.id.bt_update);
	}

	@Override
	protected void initEvents() {
		bt_update.setOnClickListener(this);
		iv_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		tv_title.setText("关于新丝路");

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_update:
			// 获取当前版本号
			PackageManager packageManager = getPackageManager();
			PackageInfo packInfo = null;
			try {
				packInfo = packageManager.getPackageInfo(getPackageName(), 0);
			} catch (NameNotFoundException e) {
				e.printStackTrace();
			}
			String oldVersion = packInfo.versionName;
			if (2 > 1) {
				final CRDialog dialog = new CRDialog(mContext);
				dialog.show("版本有更新，去更新？", "取消", "确定");
				dialog.setOnCheckedListener(new OnCheckedListener() {
					@Override
					public void onConfirm() {
						Intent i = new Intent(AboutActivity.this, UpdateService.class);
						i.putExtra("url", "");
						startService(i);
						dialog.dismiss();
					}

					@Override
					public void onCancel() {
						dialog.dismiss();
					}
				});
			} else {
				CRToast.show(mContext, "当前版本已是最新版本");
			}
			break;
		default:
			break;
		}
	}

}
