package com.XDApp.xdbase.activitys;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.GloableParams;
import com.XDApp.xdbase.UIManager;
import com.XDApp.xdbase.bean.UpdateInfo;
import com.XDApp.xdbase.dao.SharePrefUtils;
import com.XDApp.xdbase.dialog.DialogUtil;
import com.XDApp.xdbase.fragment.HomeFragment;
import com.XDApp.xdbase.fragment.MoreFragment;
import com.XDApp.xdbase.fragment.MyFragment;
import com.XDApp.xdbase.fragment.OrderFragment;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Logger;
import com.XDApp.xdbase.utils.myutils.PromptManager;
import com.XDApp.xdbase.xinsilu.R;

public class MainActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = "MainActivity";
	private ImageView home;
	private ImageView order;
	private ImageView myself;
	private ImageView more;

	// private TextView title;

	private HomeFragment homeFragment;
	private OrderFragment orderFragment;
	private MyFragment myFragment;
	private MoreFragment moreFragment;

	private Bundle bundle;

	// 更改推送
	private TextView tv_main_jpush_message;
	private int messageNum;

	private String[] listUrl;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_main);
		GloableParams.MAIN = this;
		init();
		setListener();
		// addHome();
	}

	private void init() {
		tv_main_jpush_message = (TextView) findViewById(R.id.tv_main_jpush_message);
		messageNum = SharePrefUtils.getInt(this, "messageNum", 0);

		// 初始化各个fragment
		homeFragment = new HomeFragment();
		orderFragment = new OrderFragment();
		myFragment = new MyFragment();
		moreFragment = new MoreFragment();

		initBottom();
		home.setImageResource(getImageId(0, true));
		addHome();
	}

	@Override
	protected void initEvents() {
		Logger.i(TAG, "检查版本");
//		checkVersion();

		checkNewMessage();

		Logger.i(TAG, "观察者");
		// PushMessageManager.getInstrance().addObserver(this);

	}

	/**
	 * 检查是否有新消息
	 */
	private void checkNewMessage() {
		Logger.i(TAG, "检查新消息" + messageNum);
		if (messageNum == 0) {
			tv_main_jpush_message.setVisibility(View.INVISIBLE);
		} else {
			tv_main_jpush_message.setText("" + messageNum);
			tv_main_jpush_message.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * 检查版本更新
	 */
	private void checkVersion() {
		String newVersion = null;
		Logger.i(TAG, "***********************");
		if (mApplication.updateInfo != null) {
			Logger.i(TAG, "非空");
			newVersion = mApplication.updateInfo.getVersion();
			if (newVersion != null) {
				if (!newVersion.equals(CommonUtil.getVersion(this))) {
					// TODO
					Logger.i(TAG, "弹出更新对话框");
				}
			}
		} else {
			Logger.i(TAG, "空");
			mApplication.updateInfo = new UpdateInfo();
			/****************** 假数据检车版本更新 *****************************/
			mApplication.updateInfo.setVersion("2.0");
			mApplication.updateInfo.setDescription("各种新特性，快来下载啊");

			newVersion = mApplication.updateInfo.getVersion();
			String content = mApplication.updateInfo.getDescription();
			if (newVersion != null) {
				if (!newVersion.equals(CommonUtil.getVersion(this))) {
					Logger.i(TAG, "假数据：弹出更新对话框");
					DialogUtil.createDialog(this, null, null, "更新提示", content, "马上更新", "下次再说").show();
				}
			}
		}
	}

	private void setListener() {
		home.setOnClickListener(this);
		order.setOnClickListener(this);
		myself.setOnClickListener(this);
		more.setOnClickListener(this);
	}

	private void addOrder() {
		UIManager.getInstance().changeFragment(orderFragment, false, null);
	}

	private void addHome() {
		UIManager.getInstance().changeFragment(homeFragment, false, null);
	}

	private void addMy() {
		UIManager.getInstance().changeFragment(myFragment, false, null);
	}

	private void addMore() {
		UIManager.getInstance().changeFragment(moreFragment, false, null);
	}

	private void initBottom() {
		home = (ImageView) findViewById(R.id.ii_bottom_home);
		order = (ImageView) findViewById(R.id.ii_bottom_channel);
		myself = (ImageView) findViewById(R.id.ii_bottom_search);
		more = (ImageView) findViewById(R.id.ii_bottom_lottery_myself);
	}

	/**
	 * 返回键处理
	 */
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		PromptManager.showExitSystem(this);
	}

	/**
	 * 切换底部导航图片
	 */
	@Override
	public void onClick(View v) {
		home.setImageResource(getImageId(0, false));
		order.setImageResource(getImageId(1, false));
		myself.setImageResource(getImageId(2, false));
		more.setImageResource(getImageId(3, false));

		switch (v.getId()) {
		case R.id.ii_bottom_home:
			home.setImageResource(getImageId(0, true));
			addHome();
			break;
		case R.id.ii_bottom_channel:
			order.setImageResource(getImageId(1, true));
			addOrder();
			break;
		case R.id.ii_bottom_search:
			myself.setImageResource(getImageId(2, true));
			addMy();
			break;
		case R.id.ii_bottom_lottery_myself:
			addMore();
			more.setImageResource(getImageId(3, true));
			break;
		}
	}

	/**
	 * 通过状态显示图片状态
	 * 
	 * @param paramInt
	 * @param paramBoolean
	 *            图片状态
	 * @return
	 */
	private int getImageId(int paramInt, boolean paramBoolean) {
		switch (paramInt) {
		default:
			return -1;
		case 0:
			if (paramBoolean)
				return R.drawable.ic_tab_home_press;
			return R.drawable.ic_tab_home;
		case 1:
			if (paramBoolean)
				return R.drawable.ic_tab_order_press;
			return R.drawable.ic_tab_order;
		case 2:
			if (paramBoolean)
				return R.drawable.ic_tab_my_press;
			return R.drawable.ic_tab_my;
		case 3:
			if (paramBoolean)
				return R.drawable.ic_tab_more_press;
			return R.drawable.ic_tab_more;
		}
	}

//	/**
//	 * 观察者更新
//	 */
//	@Override
//	public void update(Observable observable, Object data) {
//		/*************** 测试 ********************/
//		Logger.i(TAG, ":" + messageNum);
//		messageNum++;
//		SharePrefUtils.setint(this, "messageNum", messageNum);
//
//		/***********************************/
//		checkNewMessage();
//	}

	private long exitTime = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
			if ((System.currentTimeMillis() - exitTime) > 2000) {
				Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
				exitTime = System.currentTimeMillis();
			} else {
				finish();
				System.exit(0);
			}
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
