package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Handler;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.adapter.PersonAdapter;
import com.XDApp.xdbase.bean.PersonInfo;
import com.XDApp.xdbase.dao.SharePrefUtils;
import com.XDApp.xdbase.utils.Logger;
import com.XDApp.xdbase.utils.myutils.PromptManager;
import com.XDApp.xdbase.view.XListView;
import com.XDApp.xdbase.view.XListView.IXListViewListener;
import com.XDApp.xdbase.xinsilu.R;

public class MyListActivity extends BaseActivity implements IXListViewListener {
	private static final String TAG = "MyListActivity";
	private XListView mListView;

	private Handler mHandler;
	// private static int refreshCnt = 0;
	// private MyListAdapter mAdapter;
	private PersonAdapter mAdapter;
	private List<PersonInfo> datas;

	// private CheckBox cb_auto_refresh;
	// private boolean isCheck ;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_mylist);
		// cb_auto_refresh = (CheckBox) findViewById(R.id.cb_auto_refresh);
		mListView = (XListView) findViewById(R.id.xListView);
	}

	@Override
	protected void initEvents() {
		datas = new ArrayList<PersonInfo>();

		filldate();
		if (datas == null) {
			Logger.i(TAG, "datas为空");
		} else {
			Logger.i(TAG, "datas不为空");
		}
		// TODO
		// mListView.setAdapter(mAdapter);
		mListView.setPullLoadEnable(true);
		mListView.setXListViewListener(this);
		mHandler = new Handler();
		// cb_auto_refresh.setOnCheckedChangeListener(this);
		// isCheck = cb_auto_refresh.isChecked();
	}

	/**
	 * 异步加载数据
	 */
	private void filldate() {
		putAsyncTask(new AsyncTask<Void, Void, Boolean>() {

			@Override
			protected void onPreExecute() {
				PromptManager.showProgressDialog(MyListActivity.this);
				super.onPreExecute();
			}

			@Override
			protected Boolean doInBackground(Void... params) {
				fillmoredate(20, true);
				// try {
				// wait(2000);
				// } catch (InterruptedException e) {
				// //
				// e.printStackTrace();
				// }
				return null;
			}

			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				PromptManager.closeProgressDialog();
				mAdapter = new PersonAdapter(mApplication, getApplicationContext(), datas);
				mListView.setAdapter(mAdapter);

			}
		});
	}

	// private void geneItems() {
	// for (int i = 0; i != 5; ++i) {
	// items.add("refresh cnt " + (++start));
	// }
	// }

	/**
	 * 假数据加载
	 */
	public void fillmoredate(int count, boolean poi) {
		PersonInfo person;
		for (int i = 1; i < count; i++) {
			Logger.i(TAG, "i:" + i);
			person = new PersonInfo();
			person.setTitle("title" + i);
			person.setDescription("description" + i);
			person.setTime("time" + i);
			person.setPic(i + ".jpg");
			if (poi) {
				datas.add(0, person);
			} else {
				datas.add(person);
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		Logger.i(TAG, "autoloading" + SharePrefUtils.getBoolean(getApplicationContext(), "autoloading", true));
		mListView.setAuto(SharePrefUtils.getBoolean(getApplicationContext(), "autoloading", true));
	}

	private void onLoad() {
		mListView.stopRefresh();
		mListView.stopLoadMore();
		mListView.setRefreshTime("刚刚");
	}

	@Override
	public void onRefresh() {

		// mHandler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// start = ++refreshCnt;
		//
		// items.clear();
		// geneItems();
		// mAdapter.notifyDataSetChanged();
		// // mAdapter = new ArrayAdapter<String>(XListViewActivity.this,
		// // R.layout.list_item, items);
		// // mListView.setAdapter(mAdapter);
		// onLoad();
		// }
		// }, 2000);
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				fillmoredate(3, true);
				mAdapter.notifyDataSetChanged();
				onLoad();
			}
		}, 2000);
	}

	@Override
	public void onLoadMore() {
		// if (isCheck) {
		//
		// }
		//
		// mHandler.postDelayed(new Runnable() {
		// @Override
		// public void run() {
		// geneItems();
		// mAdapter.notifyDataSetChanged();
		// onLoad();
		// }
		// }, 2000);
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				fillmoredate(5, false);
				mAdapter.notifyDataSetChanged();
				onLoad();
			}
		}, 2000);
	}
}
