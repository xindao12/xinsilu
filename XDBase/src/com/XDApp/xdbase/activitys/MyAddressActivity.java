package com.XDApp.xdbase.activitys;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.bean.Contact;
import com.XDApp.xdbase.bean.MyContactEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 常用邮寄地址
 * @author Administrator
 *
 */
public class MyAddressActivity extends BaseActivity implements OnClickListener {
	private TextView tv_title;
	private EditText et_name, et_phone, et_address;
	private Button bt_ensure;
	private ImageView iv_back;

	/**　终端类型*/
	private final String osType = "ANDROID";
	/**　用户id*/
	private String userId = "";

	private BaseApplication mApplication;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_myaddress);
		tv_title = (TextView) findViewById(R.id.title_text);
		iv_back = (ImageView) findViewById(R.id.title_back);
		et_name = (EditText) findViewById(R.id.et_name);
		et_phone = (EditText) findViewById(R.id.et_phone);
		et_address = (EditText) findViewById(R.id.et_address);
		bt_ensure = (Button) findViewById(R.id.bt_queren);

	}

	@Override
	protected void initEvents() {
		bt_ensure.setOnClickListener(this);
		iv_back.setOnClickListener(this);
		tv_title.setText("常用邮寄地址");

		mApplication = BaseApplication.getApplication();
		userId = mApplication.getUser().getUserID();

		getData();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_queren:
			String name = et_name.getText().toString().trim();
			String phone = et_phone.getText().toString().trim();
			String address = et_address.getText().toString().trim();
			if (TextUtils.isEmpty(name)) {
				CRToast.show(MyAddressActivity.this, "名字不能为空");
				return;
			}
			if (TextUtils.isEmpty(phone)) {
				CRToast.show(MyAddressActivity.this, "电话不能为空");
				return;
			}
			if (TextUtils.isEmpty(address)) {
				CRToast.show(MyAddressActivity.this, "地址不能为空");
				return;
			}
			getQueRenData(name, phone, address);
			break;
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}

	private void getData() {
		if (CommonUtil.checkNetState(mContext)) {
			String url = xdConfig.GET_MYSENDADDRESS;
			RequestParams params = new RequestParams();
			params.put("ostype", osType);
			params.put("userID", userId);

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(url, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					// {"state":1,"status":1,"msg":"get my send address sucess",
					// "data":{"list":[{"name":"18510239152","phone":"18510239152","address":""}]}}
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();
					JSONObject obj;
					try {
						obj = new JSONObject(content);
						String state = obj.getString("state");
						String msg = obj.getString("msg");

						// 判断返回数据是否正确
						if (state.equals("1")) {
							String data = obj.getString("data");
							MyContactEntity entity = JSON.parseObject(data, MyContactEntity.class);
							if (null == entity) {
								return;
							}
							List<Contact> list = entity.getList();
							if (!TextUtils.isEmpty(list.get(0).getName())) {
								et_name.setText(list.get(0).getName());
							}
							if (!TextUtils.isEmpty(list.get(0).getPhone())) {
								et_phone.setText(list.get(0).getPhone());
							}
							if (!TextUtils.isEmpty(list.get(0).getAddress())) {
								et_address.setText(list.get(0).getAddress());
							}
							// 关闭掉这个Activity
						} else {
							CommonUtil.showToast(mContext, msg);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();

				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}

	private void getQueRenData(String name, String phone, String address) {
		if (CommonUtil.checkNetState(mContext)) {
			String url = xdConfig.UPDATE_SENDERADDRESS;
			RequestParams params = new RequestParams();
			params.put("userID", userId);
			params.put("ostype", osType);
			params.put("phone", phone);
			params.put("name", name);
			params.put("address", address);

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(url, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();
					JSONObject obj;
					try {
						obj = new JSONObject(content);
						String state = obj.getString("state");
						// 判断返回数据是否正确
						if (state.equals("1")) {
							CRToast.show(MyAddressActivity.this, "修改成功");
							finish();
						} else {
							CRToast.show(MyAddressActivity.this, "修改失败");
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}
}
