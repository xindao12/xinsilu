package com.XDApp.xdbase.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.XDApp.xdbase.xinsilu.R;

/**
 * 常见问题页面
 * @author chenghao
 *
 */
public class QuestionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question);
		((TextView) findViewById(R.id.name))
				.setText("我可以注册为会员后帮助别人购买新丝路网产品吗？\n    可以，在购买物品后，在“预订”页面填写相应的人员信息即可。\n\n购物流程：\n    如果购买您想要的产品,您可以通过登录会员中心，在网站找到您想要购买的产品页面，购买门票及旅游产品，点击选择日期及数量，购买特产商品选择份数，点击预订，选择支付方式，并且填写用户信息，完成支付就可以了。温馨提示：请在下单48小时之内支付哦。\n\n在线支付：\n    购物时有支付宝在线支付方式，用您的支付宝账号密码完成在线支付即可。\n\n电话订购：\n    拨打客服电话，4006029680/029-89522126，我们有工作人员耐心指导您完成购物。\n\n快递发货：\n    为了减少您的麻烦，您购买的产品需要快递，请在下单，填写配送地址时填写清楚您的快递地址，收件人，联系方式。如有某快递公司不到的地方，请提前备注说明，我们的工作人员为您安排适合的快递公司。\n\n送货上门：\n    如果您购买的产品需要送货上门，请在下单，填写配送地址时填写清楚您的配送地址，收件人，联系方式。我们的配送人员会提前跟您联系预约时间，完成配送。\n\n帮助中心：\n    如果您在浏览购物中遇到任何问题，可以拨打我们的客服电话4006029680 / 029-89522126，我们的工作人员耐心为您解答。\n\n联系我们：\n公司名称：陕西新丝路科技发展有限公司\n公司地址：西安市高新区高新六路38号腾飞创新中心A座615\n客服电话：4006029680 / 029-89522126");
		((TextView) findViewById(R.id.title_text)).setText("常见问题");
		findViewById(R.id.title_back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
