package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.CollectFreeAdapter;
import com.XDApp.xdbase.adapter.CollectSpeAdapter;
import com.XDApp.xdbase.adapter.CollectTicketAdapter;
import com.XDApp.xdbase.bean.CollectList;
import com.XDApp.xdbase.bean.DataEntity;
import com.XDApp.xdbase.bean.StoreEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Constants;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.view.PullToRefreshListView;
import com.XDApp.xdbase.view.PullToRefreshListView.OnPullDownRefreshListener;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 我的收藏页面
 * @author Administrator
 *
 */
public class MyFavoriteActivity extends BaseActivity implements OnClickListener {

	private PullToRefreshListView listview;
	/** 自由行List*/
	private final List<CollectList> freeList = new ArrayList<CollectList>();
	/** 自由行页数*/
	private final int vPageNum = 0;
	/** 自由行总数*/
	private final int vTotalCount = 0;

	/** 参团List*/
	private final List<CollectList> toursList = new ArrayList<CollectList>();
	/** 参团页数*/
	private final int tPageNum = 0;
	/** 参团总数*/
	private final int tTotalCount = 0;

	/** 门票List*/
	private final List<CollectList> ticketList = new ArrayList<CollectList>();
	/** 门票页数*/
	private final int tiPageNum = 0;
	/** 门票总数*/
	private final int tiTotalCount = 0;

	/** 特产List*/
	private final List<CollectList> specialList = new ArrayList<CollectList>();
	/** 特产页数*/
	private final int sPageNum = 0;
	/** 特产总数*/
	private final int sTotalCount = 0;

	private final List<List<CollectList>> lists = new ArrayList<List<CollectList>>();
	private final int[] pages = { vPageNum, tPageNum, tiPageNum, sPageNum };
	private final int[] totalCounts = { vTotalCount, tTotalCount, tiTotalCount, sTotalCount };
	/** 0参团 1自由行2 门票 3 特产*/
	private final int[] favTypes = { 2, 0, 1, 3 };

	/** 当前*/
	private int currentIndex = 0;

	private boolean isEmp1 = true, isEmp2 = true, isEmp3 = true;
	private boolean isEmp4 = true;

	private String userId;

	private TextView tvTab1, tvTab2, tvTab3, tvTab4;
	private View v1, v2, v3, v4;
	private SharedPreferences sp;

	private CollectSpeAdapter speAdapter;
	private CollectTicketAdapter ticketAdapter;
	private CollectFreeAdapter freeAdapter;
	private CollectFreeAdapter toursAdapter;

	// 标题控件
	private ImageView back;
	private TextView title;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_myfavorite);

		sp = getSharedPreferences(MainActivity.class.getSimpleName(), MODE_PRIVATE);
		userId = sp.getString("userID", "");

		listview = (PullToRefreshListView) findViewById(R.id.listview);
		tvTab1 = (TextView) findViewById(R.id.tab_1);
		tvTab2 = (TextView) findViewById(R.id.tab_2);
		tvTab3 = (TextView) findViewById(R.id.tab_3);
		tvTab4 = (TextView) findViewById(R.id.tab_4);

		v1 = findViewById(R.id.view_1);
		v2 = findViewById(R.id.view_2);
		v3 = findViewById(R.id.view_3);
		v4 = findViewById(R.id.view_4);

		tvTab1.setOnClickListener(this);
		tvTab2.setOnClickListener(this);
		tvTab3.setOnClickListener(this);
		tvTab4.setOnClickListener(this);

		lists.add(ticketList);
		lists.add(toursList);
		lists.add(freeList);
		lists.add(specialList);

		speAdapter = new CollectSpeAdapter(this, specialList, R.layout.activity_freewalker_item);
		ticketAdapter = new CollectTicketAdapter(this, ticketList, R.layout.activity_ticket_item);
		freeAdapter = new CollectFreeAdapter(this, freeList, "0", R.layout.activity_freewalker_item);
		toursAdapter = new CollectFreeAdapter(this, toursList, "1", R.layout.activity_freewalker_item);

//		listview.setAdapter(ticketAdapter);
		// 标题控件
		back = (ImageView) findViewById(R.id.title_back);
		back.setOnClickListener(this);
		title = (TextView) findViewById(R.id.title_text);
		title.setText("我的收藏");
	}

	@Override
	protected void initEvents() {

		initData();

		listview.setonRefreshListener(new OnPullDownRefreshListener() {

			@Override
			public void onRefresh() {

			}

			@Override
			public void onLoadMoring() {

				int nowNum = lists.get(currentIndex).size();
				int totalNum = totalCounts[currentIndex];

				if (nowNum < totalNum) {
					pages[currentIndex]++;
					initData();
				}
				listview.onRefreshFinish();
			}
		});

		listview.setRefreshable(false);

	}

	private void initData() {
		if (CommonUtil.checkNetState(mContext)) {
			final RequestParams params = new RequestParams();
			params.put("favType", "" + favTypes[currentIndex]);
			params.put("pageNum", "" + pages[currentIndex]);
			params.put("pageSize", "" + Constants.PAGE_SIZE);
			params.put("userID", "" + userId);
			params.put("ostype", "ANDROID");

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);

			XDHttpClient.post(Constants.STORE_INFOR, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();

					if (TextUtils.isEmpty(content)) {
						return;
					}
					DataEntity entity = JSON.parseObject(content, DataEntity.class);
					if ("1".equals(entity.getState())) {// 成功
						StoreEntity se;
						List<CollectList> list;
						switch (currentIndex) {
						case 0:
							se = JSON.parseObject(entity.getData(), StoreEntity.class);
							list = se.getList();
							if (list != null) {
								isEmp1 = false;
								if (list.size() != 0) {
//									lists.get(currentIndex).addAll(list);
									ticketAdapter.addData(list);
								} else {
									isEmp1 = true;
								}
							}
							listview.setAdapter(ticketAdapter);
							totalCounts[currentIndex] = se.getTotalCount();
							break;
						case 1:
							se = JSON.parseObject(entity.getData(), StoreEntity.class);
							list = se.getList();
							if (list != null) {
								isEmp2 = false;
								if (list.size() != 0) {
//									lists.get(currentIndex).addAll(list);
									toursAdapter.addData(list);
								} else {
									isEmp2 = true;
								}
							}
							listview.setAdapter(toursAdapter);
							totalCounts[currentIndex] = se.getTotalCount();
							break;
						case 2:
							se = JSON.parseObject(entity.getData(), StoreEntity.class);
							list = se.getList();
							if (list != null) {
								isEmp3 = false;
								if (list.size() != 0) {
//									lists.get(currentIndex).addAll(list);
									freeAdapter.addData(list);
								} else {
									isEmp3 = true;
								}
							}
							listview.setAdapter(freeAdapter);
							totalCounts[currentIndex] = se.getTotalCount();
							break;
						case 3:
							se = JSON.parseObject(entity.getData(), StoreEntity.class);
							list = se.getList();
							if (list != null) {
								isEmp4 = false;
								if (list.size() != 0) {
//									lists.get(currentIndex).addAll(list);
									speAdapter.addData(list);
								} else {
									isEmp4 = true;
								}
							}
							listview.setAdapter(speAdapter);
							totalCounts[currentIndex] = se.getTotalCount();
							break;
						}
					} else {
						CommonUtil.showToast(mContext, entity.getMsg());
					}
				}

				@Override
				public void onFailure(Throwable error, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();

					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
				}

			});
		} else {// 提示网络未连接
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}

	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tab_1:
			if (currentIndex == 0)
				return;
			currentIndex = 0;
			v1.setBackgroundResource(R.drawable.bg_sj);
			v2.setBackgroundResource(R.drawable.bg_title_default);
			v3.setBackgroundResource(R.drawable.bg_title_default);
			v4.setBackgroundResource(R.drawable.bg_title_default);

			if (isEmp1) {
				initData();
			} else {
				listview.setAdapter(ticketAdapter);
			}

			break;
		case R.id.tab_2:
			if (currentIndex == 1)
				return;
			currentIndex = 1;
			v1.setBackgroundResource(R.drawable.bg_title_default);
			v2.setBackgroundResource(R.drawable.bg_sj);
			v3.setBackgroundResource(R.drawable.bg_title_default);
			v4.setBackgroundResource(R.drawable.bg_title_default);

			if (isEmp2) {
				initData();
			} else {
				listview.setAdapter(toursAdapter);
			}

			break;
		case R.id.tab_3:
			if (currentIndex == 2)
				return;
			currentIndex = 2;
			v1.setBackgroundResource(R.drawable.bg_title_default);
			v2.setBackgroundResource(R.drawable.bg_title_default);
			v3.setBackgroundResource(R.drawable.bg_sj);
			v4.setBackgroundResource(R.drawable.bg_title_default);

			if (isEmp3) {
				initData();
			} else {
				listview.setAdapter(freeAdapter);
			}

			break;
		case R.id.tab_4:
			if (currentIndex == 3)
				return;
			currentIndex = 3;
			v1.setBackgroundResource(R.drawable.bg_title_default);
			v2.setBackgroundResource(R.drawable.bg_title_default);
			v3.setBackgroundResource(R.drawable.bg_title_default);
			v4.setBackgroundResource(R.drawable.bg_sj);

			if (isEmp4) {
				initData();
			} else {
				listview.setAdapter(speAdapter);
			}

			break;
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		speAdapter.notifyDataSetChanged();
		ticketAdapter.notifyDataSetChanged();
		freeAdapter.notifyDataSetChanged();
		toursAdapter.notifyDataSetChanged();
	}
}
