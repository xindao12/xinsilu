package com.XDApp.xdbase.activitys;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.xinsilu.R;

/**
 * 自由行详情页的webview
 * @author Administrator
 *
 */
public class FreeWalkerDataActivity extends BaseActivity {
	private ImageView iv_back;
	private TextView tv_title;
	private WebView wv_data;
	private String data;
	private int type;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_freewalker_data);
		data = getIntent().getStringExtra("data");
		type = getIntent().getIntExtra("type", -1);
		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);
		wv_data = (WebView) findViewById(R.id.wv_data);

	}

	@Override
	protected void initEvents() {
		switch (type) {
		case 0:
			tv_title.setText("产品特色");
			break;
		case 1:
			tv_title.setText("费用说明");
			break;
		case 2:
			tv_title.setText("重要提示");
			break;
		case 3:
			tv_title.setText("用户点评");
			break;
		default:
			break;
		}
		if (TextUtils.isEmpty(data)) {
			data = "暂无详情";
		}
		wv_data.getSettings().setDefaultTextEncodingName("UTF-8");
		wv_data.loadData(data, "text/html; charset=UTF-8", null);
		iv_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
