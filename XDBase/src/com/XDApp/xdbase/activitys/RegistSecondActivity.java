package com.XDApp.xdbase.activitys;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class RegistSecondActivity extends BaseActivity implements OnClickListener {
	public static final String PARAM_PHONE = "phone";
	private final String TAG = "RegistSecondActivity";
	private ImageView iv_back;
	private TextView tv_title;
	private TextView tv_phone;
	private Button bt_goto;
	private EditText et_username;
	private SharedPreferences sp;
	private BaseApplication application;
	private String phone;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_regist_second);
		application = BaseApplication.getApplication();
		sp = getSharedPreferences(MainActivity.class.getSimpleName(), MODE_PRIVATE);
		phone = getIntent().getStringExtra("phone");

		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);
		tv_phone = (TextView) findViewById(R.id.tv_phone);
		bt_goto = (Button) findViewById(R.id.bt_registsecond);
		et_username = (EditText) findViewById(R.id.username);

	}

	@Override
	protected void initEvents() {
		tv_phone.setText("接收验证码的手机号为" + phone);
		bt_goto.setOnClickListener(this);
		bt_goto.setClickable(false);
		iv_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		if (getIntent().getBooleanExtra(RegistFirstActivity.PARAM_IS_REGISTER, true)) {
			tv_title.setText("快速注册");
		} else {
			tv_title.setText("验证验证码");
		}

		et_username.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				bt_goto.setBackgroundResource(R.drawable.bt_login_normal);
				bt_goto.setTextColor(Color.parseColor("#777777"));
				bt_goto.setClickable(false);
			}

			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(et_username.getText().toString().trim())) {
					bt_goto.setBackgroundResource(R.drawable.bt_login_normal);
					bt_goto.setTextColor(Color.parseColor("#777777"));
					bt_goto.setClickable(false);
					return;
				}
				bt_goto.setBackgroundResource(R.drawable.bt_login_press);
				bt_goto.setTextColor(Color.WHITE);
				bt_goto.setClickable(true);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_registsecond:// 下一步
			String code = et_username.getText().toString().trim();
			getData(getIntent().getBooleanExtra("is_email", false), phone, code);
			break;
		default:
			break;
		}
	}

	// TODO 访问网络获取数据
	private void getData(boolean isEmail, final String phone, String code) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			if (isEmail) {
				params.put("email", phone);
			} else {
				params.put("phoneNum", phone);
			}
			params.put("verifyCode", code);

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(xdConfig.CHECKVERIFYCODE, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();
					Log.e(TAG, content);
					// 判断返回数据是否正确
					if (TextUtils.isEmpty(content)) {
						CommonUtil.showToast(mContext, "获取数据失败");
						return;
					}

					JSONObject obj = new JSONObject().parseObject(content);
					String state = obj.getString("state");
					if ("1".equals(state)) {
						Intent intent = null;
						if (getIntent().getBooleanExtra(RegistFirstActivity.PARAM_IS_REGISTER, true)) {
							intent = new Intent(RegistSecondActivity.this, RegistThirdActivity.class);
						} else {
							intent = new Intent(RegistSecondActivity.this, ResetPwdActivity.class);
						}
						intent.putExtra("phone", phone);
						startActivity(intent);
						finish();
					} else {
						et_username.setText("");
						CommonUtil.showToast(RegistSecondActivity.this, "验证码错误!");
						return;
					}

				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();

				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}
}
