package com.XDApp.xdbase.activitys;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.SpecialAdapter;
import com.XDApp.xdbase.bean.Product;
import com.XDApp.xdbase.bean.SpecialEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 特产商城列表
 * @author Administrator
 *
 */
public class SpecialActivity extends BaseActivity implements OnClickListener {
	private TextView title_text;
	private ImageView iv_back;
	private PullToRefreshListView listView;
	private SpecialAdapter adapter;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_special);
		title_text = (TextView) findViewById(R.id.title_text);
		iv_back = (ImageView) findViewById(R.id.title_back);
		listView = (PullToRefreshListView) findViewById(R.id.listView);
		listView.setOnItemClickListener(mOnItemClickListener);
		iv_back.setOnClickListener(this);

		title_text.setText("特产商城");
		initData();
		refreshAndLoadMore();
	}

	private final OnItemClickListener mOnItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = new Intent(mContext, SeneryDetailActivity.class);
			intent.putExtra(SeneryDetailActivity.PARAM_PRODUCTID,
					((Product) parent.getItemAtPosition(position)).getProductId());
			startActivity(intent);
		}
	};

	private void initData() {
		getData(mContext, 1, xdConfig.LIMIT, new ILoadNextPageData<Product>() {

			@Override
			public void loadNextPageData(List<Product> t) {
				if (null != t) {
					adapter = new SpecialAdapter(mContext, t, xdConfig.LIMIT,
							R.layout.activity_freewalker_item, R.layout.item_loading);
					listView.setAdapter(adapter);
					listView.setPullLabel("加载成功");
//					listView.onLoadSuccess();
				} else {
					listView.setPullLabel("加载失败");
//					CRToast.show(mContext, "获取数据失败");
//					listView.onLoadFailed();
				}
				listView.onRefreshComplete();
			}
		});
	}

	private void refreshAndLoadMore() {
		listView.setMode(Mode.PULL_FROM_START);
		listView.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
//				listView.onRefreshComplete();
				initData();
			}
		});
	}

	// TODO 访问网络获取数据
	private static void getData(final Context mContext, int pageNumber, int pageSize,
			final ILoadNextPageData<Product> iNextPageData) {
//		if (CommonUtil.checkNetState(mContext)) {
		RequestParams params = new RequestParams();
		params.put("pageNum", String.valueOf(pageNumber));
		params.put("pageSize", String.valueOf(xdConfig.LIMIT));

		XDHttpClient.post(xdConfig.GET_PRODUCTLIST, params, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, String content) {

				JSONObject obj = new JSONObject().parseObject(content);
				String data = obj.getString("data");
				if (TextUtils.isEmpty(data)) {
					CommonUtil.showToast(mContext, "获取数据失败");
					return;
				}

				SpecialEntity parseObject = JSON.parseObject(data, SpecialEntity.class);

				// 判断返回数据是否正确
				if (null != parseObject) {
					iNextPageData.loadNextPageData(parseObject.getProductList());
					// 关闭掉这个Activity
				} else {
					iNextPageData.loadNextPageData(null);
					// CommonUtil.showToast(mContext, "获取数据失败");
				}
			}

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				iNextPageData.loadNextPageData(null);
			}
		});
//		} else {
//			iNextPageData.loadNextPageData(null);
		// Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
//		}
	}

	@Override
	protected void initEvents() {
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				// Intent intent=new
				// Intent(TicketActivity.this,FreeWalkerDetailActivity.class);
				// intent.putExtra("ticketID",
				// list.get(position).getTicketId());
				// startActivity(intent);
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_back:
			finish();
			break;
		default:
			break;
		}
	}
}
