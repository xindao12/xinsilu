package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.MyStoreAdapter;
import com.XDApp.xdbase.bean.CollectList;
import com.XDApp.xdbase.bean.StoreEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Constants;
import com.XDApp.xdbase.view.PullToRefreshListView;
import com.XDApp.xdbase.view.PullToRefreshListView.OnPullDownRefreshListener;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MyStoreActivity extends BaseActivity implements OnClickListener {
	private RelativeLayout tvPrice, tvWalk, tvGroup, tvSpecial;
	private PullToRefreshListView listview;
	private List<CollectList> lists = new ArrayList<CollectList>();
	private int favType = 0;// 0参团 1自由行2 门票 3 特产
	private int pageNum = 0;
	private int totalCount;
	private MyStoreAdapter adapter;
	private ImageView imageView;
	private TextView title;
	private ImageView arrow1, arrow2, arrow3, arrow4;
	private int offset;
	private int bmpW;
	private int one;
	private int two;
	private int three;
	private TranslateAnimation animation;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_my_store);
		listview = (PullToRefreshListView) findViewById(R.id.listview_my_store);
		tvPrice = (RelativeLayout) findViewById(R.id.relativelayout1);
		tvPrice.setOnClickListener(this);
		tvWalk = (RelativeLayout) findViewById(R.id.relativelayout3);
		tvWalk.setOnClickListener(this);
		tvGroup = (RelativeLayout) findViewById(R.id.relativelayout2);
		tvGroup.setOnClickListener(this);
		tvSpecial = (RelativeLayout) findViewById(R.id.relativelayout4);
		tvSpecial.setOnClickListener(this);
		imageView = (ImageView) findViewById(R.id.imageview);
		title = (TextView) findViewById(R.id.title_text);
		title.setText("我的收藏");
		arrow1 = (ImageView) findViewById(R.id.imageview_below1);
		arrow2 = (ImageView) findViewById(R.id.imageview_below2);
		arrow3 = (ImageView) findViewById(R.id.imageview_below3);
		arrow4 = (ImageView) findViewById(R.id.imageview_below4);
		InitImageView();
		listview.setonRefreshListener(new OnPullDownRefreshListener() {

			@Override
			public void onRefresh() {
				pageNum = 0;
				if (null != lists && lists.size() > 0) {
					lists.clear();
				}
				initData();
			}

			@Override
			public void onLoadMoring() {
				pageNum++;
				if ((pageNum + 1) * Constants.PAGE_SIZE < totalCount) {
					initData();
				} else {
					listview.onRefreshComplete();
				}
			}
		});

	}

	@Override
	protected void initEvents() {
		// initData();
	}

	private void initData() {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("favType", "" + favType);
			params.put("pageNum", "" + pageNum);
			params.put("pageSize", "" + Constants.PAGE_SIZE);
			params.put("userId", "" + pageNum);
			params.put("osType", "ANDROID");

			XDHttpClient.post(Constants.STORE_INFOR, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					// super.onSuccess(statusCode, content);
					listview.onRefreshComplete();
					if (TextUtils.isEmpty(content) || !TextUtils.isEmpty(content) && !content.startsWith("{")) {
						return;
					}
					StoreEntity entity = JSON.parseObject(content, StoreEntity.class);
					if ("1".equals(entity.getState())) {// 成功
						lists = entity.getList();
						totalCount = entity.getTotalCount();
						resetAdapter();
					} else {

					}
				}

				@Override
				public void onFailure(Throwable error, String content) {
					// super.onFailure(error, content);
					listview.onRefreshComplete();
					resetAdapter();
				}

			});
		} else {// 提示网络未连接
		}

	}

	private void resetAdapter() {
		if (null == adapter) {
			adapter = new MyStoreAdapter(mContext, lists);
			listview.setAdapter(adapter);
		} else {
			adapter.setStoreLists(lists);
			adapter.notifyDataSetChanged();
		}

		if (adapter.getCount() == 0) {
			imageView.setVisibility(View.VISIBLE);
		} else {
			imageView.setVisibility(View.INVISIBLE);
		}

	}

	/**
	 * 初始化动画
	 */
	private void InitImageView() {
		bmpW = BitmapFactory.decodeResource(getResources(), R.drawable.order_intro).getWidth();
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int screenW = dm.widthPixels;// 获取分辨率宽度
		offset = (screenW / 4 - bmpW) / 2;
		Matrix matrix = new Matrix();
		matrix.postTranslate(offset, 0);
		arrow1.setImageMatrix(matrix);// 设置动画初始位置
		one = offset * 2 + bmpW;
		two = one * 2;
		three = one * 3;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_group:// 参团游
			arrow1.setVisibility(View.VISIBLE);
			arrow2.setVisibility(View.INVISIBLE);
			arrow3.setVisibility(View.INVISIBLE);
			arrow4.setVisibility(View.INVISIBLE);
			// if (favType == 1) {
			// animation = new TranslateAnimation(one, 0, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 2) {
			// animation = new TranslateAnimation(two, 0, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 3) {
			// animation = new TranslateAnimation(three, 0, 0, 0);
			// arrow1.startAnimation(animation);
			// }
			favType = 0;
			if (null != lists && lists.size() > 0) {
				lists.clear();
			}
			initData();
			break;
		case R.id.tv_walk:// 自由行
			// if (favType == 0) {
			// animation = new TranslateAnimation(offset, one, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 2) {
			// animation = new TranslateAnimation(two, one, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 3) {
			// animation = new TranslateAnimation(three, one, 0, 0);
			// arrow1.startAnimation(animation);
			// }
			arrow1.setVisibility(View.INVISIBLE);
			arrow2.setVisibility(View.VISIBLE);
			arrow3.setVisibility(View.INVISIBLE);
			arrow4.setVisibility(View.INVISIBLE);
			favType = 1;
			if (null != lists && lists.size() > 0) {
				lists.clear();
			}
			initData();
			break;
		case R.id.tv_price:// 门票
			// if (favType == 0) {
			// animation = new TranslateAnimation(offset, two, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 1) {
			// animation = new TranslateAnimation(one, two, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 3) {
			// animation = new TranslateAnimation(three, two, 0, 0);
			// arrow1.startAnimation(animation);
			// }
			arrow1.setVisibility(View.INVISIBLE);
			arrow3.setVisibility(View.VISIBLE);
			arrow2.setVisibility(View.INVISIBLE);
			arrow4.setVisibility(View.INVISIBLE);
			favType = 2;
			if (null != lists && lists.size() > 0) {
				lists.clear();
			}
			// initData();
			break;
		case R.id.tv_special:// 特产
			arrow1.setVisibility(View.INVISIBLE);
			arrow4.setVisibility(View.VISIBLE);
			arrow3.setVisibility(View.INVISIBLE);
			arrow2.setVisibility(View.INVISIBLE);
			// if (favType == 0) {
			// animation = new TranslateAnimation(offset, three, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 1) {
			// animation = new TranslateAnimation(one, three, 0, 0);
			// arrow1.startAnimation(animation);
			// } else if (favType == 2) {
			// animation = new TranslateAnimation(two, three, 0, 0);
			// arrow1.startAnimation(animation);
			// }
			favType = 3;
			if (null != lists && lists.size() > 0) {
				lists.clear();
			}
			// initData();
			break;
		}
	}

}
