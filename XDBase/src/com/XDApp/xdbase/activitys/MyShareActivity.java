package com.XDApp.xdbase.activitys;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.utils.SharePopupWindow;
import com.XDApp.xdbase.xinsilu.R;
import com.umeng.socialize.controller.RequestType;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;

public class MyShareActivity extends BaseActivity implements OnClickListener {

	private Button shareButton;
	private SharePopupWindow mPopupWindow;

	final UMSocialService mController = UMServiceFactory.getUMSocialService("com.umeng.share",
			RequestType.SOCIAL);

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_share);

		shareButton = (Button) findViewById(R.id.btn_share_test);

	}

	@Override
	protected void initEvents() {

		shareButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_share_test:
			View convertView = View.inflate(this, R.layout.popup, null);
			if (null == mPopupWindow)
				mPopupWindow = new SharePopupWindow(mContext, convertView,
						getResources().getDisplayMetrics().widthPixels, LayoutParams.WRAP_CONTENT);
			mPopupWindow.setInfors(mController, "ceshi_title", "ceshi_content", "http://3g.lgmi.com/app_"
					+ "ceshi_zhuti" + ".htm");
			mPopupWindow.showAtLocation(findViewById(R.id.main), Gravity.BOTTOM, 0, 0);
			break;
		default:
			break;
		}
	}

	@Override
	protected void onDestroy() {
		if (null != mPopupWindow) {
			mPopupWindow = null;
		}
		super.onDestroy();
	}

}
