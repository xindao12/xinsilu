package com.XDApp.xdbase.activitys;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.BaseApplication;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.adapter.MyOrderAdapter;
import com.XDApp.xdbase.bean.OrderList;
import com.XDApp.xdbase.bean.OrderListEntity;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Constants;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.xinsilu.R;
import com.alibaba.fastjson.JSON;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * @author admin 我的订单
 * 
 */
public class MyOrderActivity extends BaseActivity implements OnClickListener, OnItemClickListener {
	private PullToRefreshListView listview;
	private TextView title;
	private TextView tvNoComplete;
	private TextView tvHasComplete;
	private static String url;
	private static int type = 0;// 默认为未支付订单，1为已支付订单
	private ImageView imageView;
	private ImageView ivBack;
	private final View[] mTabViews = new View[2];
	private int position;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_myorder);
		position = getIntent().getIntExtra("type", 0);
		chooseTheUrl(position);
		listview = (PullToRefreshListView) findViewById(R.id.listview_order_list);
		tvNoComplete = (TextView) findViewById(R.id.tv_no_complete);
		tvNoComplete.setOnClickListener(this);
		tvHasComplete = (TextView) findViewById(R.id.tv_has_complete);
		tvHasComplete.setOnClickListener(this);
		mTabViews[0] = tvNoComplete;
		mTabViews[1] = tvHasComplete;
		title = (TextView) findViewById(R.id.title_text);
		imageView = (ImageView) findViewById(R.id.imageview);
		ivBack = (ImageView) findViewById(R.id.title_back);
		switch (position) {
		case 0:
			title.setText("景点门票订单");
			break;
		case 1:
			title.setText("自由行订单");
			break;
		case 2:
			title.setText("参团游订单");
			break;
		case 3:
			title.setText("特产订单");
			break;
		default:
			break;
		}
		listview.setMode(Mode.PULL_FROM_START);
		listview.setOnRefreshListener(new OnRefreshListener<ListView>() {

			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {
				initDatas();
			}
		});
		listview.setOnItemClickListener(this);
		ivBack.setOnClickListener(this);
		initDatas();
		setTabSelected(0);
	}

	private void setTabSelected(int index) {
		for (int i = 0; i < mTabViews.length; i++) {
			if (index == i) {
				mTabViews[i].setSelected(true);
			} else {
				mTabViews[i].setSelected(false);
			}
		}
	}

	public static void initData(Context mContext, int pageNum, int pageSize,
			final ILoadNextPageData<OrderList> iLoadNextPageData) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			params.put("orderType", "" + type);
			params.put("pageNum", "" + pageNum);
			params.put("pageSize", "" + Constants.PAGE_SIZE);
			params.put("userID", "" + BaseApplication.getApplication().getUser().getUserID());
			params.put("ostype", xdConfig.ANDRODID);

			XDHttpClient.post(url, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					// super.onSuccess(statusCode, content);
					if (TextUtils.isEmpty(content)) {
						return;
					}
					OrderListEntity entity;
					try {
						entity = JSON.parseObject(new JSONObject(content).getJSONObject("data").toString(),
								OrderListEntity.class);
						if (1 == (new JSONObject(content).getInt("state"))) {// 成功
							iLoadNextPageData.loadNextPageData(entity.getOrderList());
						} else {
							iLoadNextPageData.loadNextPageData(null);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(Throwable error, String content) {
					// super.onFailure(error, content);
					iLoadNextPageData.loadNextPageData(null);
				}

			});
		} else {// 提示网络未连接
		}

	}

	/*private void resetAdapter() {
		if (null == adapter) {
			adapter = new MyOrderAdapter(orderLists, mContext);
			listview.setAdapter(adapter);
		} else {
			adapter.setOrderLists(orderLists);
			adapter.notifyDataSetChanged();
		}

		if (adapter.getCount() == 0) {
			imageView.setVisibility(View.VISIBLE);
		} else {
			imageView.setVisibility(View.GONE);
		}
	}*/

	private void chooseTheUrl(int position) {
		switch (position) {
		case 0:
			url = Constants.ORDER_SENERY;
			break;
		case 1:
			url = Constants.ORDER_WALKER;
			break;
		case 2:
			url = Constants.ORDER_GROUP_PLAY;
			break;
		case 3:
			url = Constants.ORDER_SPECIAL;
			break;
		}

	}

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.tv_no_complete:
			type = 0;
			initDatas();
			setTabSelected(0);
			break;
		case R.id.tv_has_complete:
			type = 1;
			initDatas();
			setTabSelected(1);
			break;
		case R.id.title_back:
			finish();
			break;
		}
	}

	private void initDatas() {
		initData(mContext, 1, xdConfig.LIMIT, new ILoadNextPageData<OrderList>() {

			@Override
			public void loadNextPageData(List<OrderList> t) {
				if (null != t) {
					listview.setAdapter(new MyOrderAdapter(mContext, type, t, xdConfig.LIMIT,
							R.layout.listview_myorder_item, R.layout.item_loading));
					listview.setPullLabel("加载成功");
				} else {
					listview.setPullLabel("加载失败");
					CommonUtil.showToast(mContext, "获取数据失败！");
				}
				listview.onRefreshComplete();
			}
		});
	}

	@Override
	protected void initEvents() {
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent intent = new Intent(MyOrderActivity.this, OrderDetailActivity.class);
		OrderList ol = (OrderList) arg0.getAdapter().getItem(arg2);
		intent.putExtra("type", position);
		intent.putExtra("orderId", ol.getOrderId());
		startActivity(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		listview.setRefreshing();
	}

}
