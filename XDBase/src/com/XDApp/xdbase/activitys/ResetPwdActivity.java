package com.XDApp.xdbase.activitys;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.XDHttpClient;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.ProgressHUD;
import com.XDApp.xdbase.utils.xdConfig;
import com.XDApp.xdbase.view.CRToast;
import com.XDApp.xdbase.xinsilu.R;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * 重置密码界面
 * @author chenghao
 *
 */
public class ResetPwdActivity extends BaseActivity implements OnClickListener {
	private TextView tv_title, tv_right;
	private EditText et_newPwd, et_againPwd;
	private String newPwd;
	private String againPwd;

	@Override
	protected void initViews() {
		setContentView(R.layout.layout_resetpwd);
		tv_title = (TextView) findViewById(R.id.title_text);
		tv_right = (TextView) findViewById(R.id.title_right);

		et_newPwd = (EditText) findViewById(R.id.newpwd);
		et_againPwd = (EditText) findViewById(R.id.againpwd);

		tv_right.setOnClickListener(this);
	}

	@Override
	protected void initEvents() {
		tv_right.setText("确定");
		tv_right.setTextColor(Color.WHITE);
		tv_title.setText("修改密码");
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.title_back:
			finish();
			break;
		case R.id.title_right:// 确定按钮
			newPwd = et_newPwd.getText().toString().trim();
			againPwd = et_againPwd.getText().toString().trim();
			if (TextUtils.isEmpty(newPwd) || TextUtils.isEmpty(againPwd)) {
				CRToast.show(ResetPwdActivity.this, "密码不能为空");
				return;
			}
			if (!newPwd.equals(againPwd)) {
				CRToast.show(ResetPwdActivity.this, "两次密码输入不一致");
				return;
			}
			if (!(newPwd.length() > 5 && newPwd.length() < 17)) {
				CRToast.show(ResetPwdActivity.this, "请输入6--16位之间的密码");
				return;
			}
			getData(newPwd);
			break;
		default:
			break;
		}
	}

	/**
	 * 判断手机号码是否合法.
	 */
	private boolean isMobilePhone(String address) {
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,2,5-9]))\\d{8}$");
		Matcher m = p.matcher(address);
		return m.matches();
	}

	private void getData(String pwd) {
		if (CommonUtil.checkNetState(mContext)) {
			RequestParams params = new RequestParams();
			String phone = getIntent().getStringExtra(RegistSecondActivity.PARAM_PHONE);
			if (isMobilePhone(phone)) {
				params.put("phone", phone);
			} else {
				params.put("email", phone);
			}
			params.put("password", LoginActivity.md5(pwd));

			final ProgressHUD ProgressDialog = ProgressHUD.show(mContext, "正在加载...", true, true, null);
			XDHttpClient.post(xdConfig.RESETPASSWORD, params, new AsyncHttpResponseHandler() {
				@Override
				public void onSuccess(int statusCode, String content) {
					if (null != ProgressDialog && ProgressDialog.isShowing())
						ProgressDialog.dismiss();
					Log.i("RestPwd", content);
					try {
						if (1 == new JSONObject(content).getInt("state")) {
							CommonUtil.showToast(mContext, "重置密码成功");
							finish();
						} else {
							CommonUtil.showToast(mContext, "重置密码失败");
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void onFailure(Throwable arg0, String arg1) {
					if (null != ProgressDialog && ProgressDialog.isShowing()) {
						ProgressDialog.dismiss();
					}
					super.onFailure(arg0, arg1);
					Toast.makeText(mContext, R.string.pLease_check_network, 0).show();

				}
			});
		} else {
			Toast.makeText(mContext, R.string.pLease_check_network, 0).show();
		}
	}
}
