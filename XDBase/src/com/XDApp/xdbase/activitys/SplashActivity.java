package com.XDApp.xdbase.activitys;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import cn.jpush.android.api.JPushInterface;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.ConstantValue;
import com.XDApp.xdbase.bean.UpdateInfo;
import com.XDApp.xdbase.dao.SharePrefUtils;
import com.XDApp.xdbase.utils.Logger;
import com.XDApp.xdbase.xinsilu.R;

public class SplashActivity extends BaseActivity {

	protected static final String TAG = "SplashActivity";
	protected static final int SERVER_ERROR = 1;
	protected static final int URL_ERROR = 2;
	protected static final int PROTOCOL_ERROR = 3;
	protected static final int NETWORK_ERROR = 4;
	protected static final int XML_PARSE_ERROR = 5;
	protected static final int PARSE_SUCCESS = 6;

	private final Handler handler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case SERVER_ERROR:
				showLongToast("服务器内部错误.");
				loadMainUI();
				break;
			case URL_ERROR:
				showLongToast("URL错误.");
				// loadMainUI();
				break;
			case PROTOCOL_ERROR:
				showLongToast("网络协议错误.");
				loadMainUI();
				break;
			case NETWORK_ERROR:
				showLongToast("网络连接错误.请检查网络");
				loadMainUI();
				break;
			case XML_PARSE_ERROR:
				showLongToast("解析xml文件失败");
				loadMainUI();
				break;
			case PARSE_SUCCESS:
				loadMainUI();
				break;
			}
		};
	};

	@Override
	protected void initViews() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_splash);

		AlphaAnimation aa = new AlphaAnimation(0.4f, 1.0f);
		aa.setDuration(ConstantValue.SPLASH_TIME);
		aa.setFillAfter(true);// 停留
		findViewById(R.id.rl_splash_root).startAnimation(aa);

	}

	@Override
	protected void initEvents() {
		pushinit();
		longtime();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				/****/
				loadMainUI();
				/****/
			}
		}, ConstantValue.SPLASH_TIME);
	}

	/**
	 * 标签别名
	 */
	private void pushinit() {
		SharePrefUtils.setBoolean(getApplicationContext(), "push", true);
		SharePrefUtils.setBoolean(getApplicationContext(), "autoloading", true);
		Set<String> tags = new HashSet<String>();
		// tags.add("andorid");
		tags.add("iso");
		JPushInterface.setAliasAndTags(getApplicationContext(), "ceshi", tags);

	}

	private void longtime() {
//		checkVersionTask();
	}

	/**
	 * 子线程 检查版本号
	 */
	private void checkVersionTask() {
		new Thread() {
			@Override
			public void run() {
				long startTime = System.currentTimeMillis();
				Message msg = Message.obtain();
				// 得到服务器的路径
				try {
					String serverurl = getString(R.string.server_url);
					URL url = new URL(serverurl);
					HttpURLConnection conn = (HttpURLConnection) url.openConnection();
					conn.setRequestMethod("POST");
					conn.setConnectTimeout(2000);
					// 得到服务器响应码
					int code = conn.getResponseCode();
					if (code == 200) {
						// 服务器返回的输入流 xml文件
						InputStream is = conn.getInputStream();
						// updateInfo = UpdateInfoParser.getUpdateInfo(is);
						// TODO
						mApplication.updateInfo = parser2updateInfo(is);
						msg.what = PARSE_SUCCESS;

					} else {
						// 请求失败
						msg.what = SERVER_ERROR;
					}
				} catch (MalformedURLException e) {
					e.printStackTrace();
					msg.what = URL_ERROR;
				} catch (ProtocolException e) {
					e.printStackTrace();
					msg.what = PROTOCOL_ERROR;
				} catch (IOException e) {
					e.printStackTrace();
					msg.what = NETWORK_ERROR;
				} finally {
					// 延时发送消息
					long endTime = System.currentTimeMillis();
					long dTime = endTime - startTime;
					if (dTime < 2000) {
						try {
							Thread.sleep(2000 - dTime);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					handler.sendMessage(msg);
				}

			}

		}.start();
	}

	/**
	 * 解析更新信息
	 * 
	 * @param is
	 * @return
	 */
	protected UpdateInfo parser2updateInfo(InputStream is) {
		Logger.i(TAG, is.toString());
		return null;
	}

	/**
	 * 进入主页面
	 */
	private void loadMainUI() {
		// 判断是否是第一次登陆
		if (SharePrefUtils.getBoolean(this, "isFirst", true)) {
			Logger.i(TAG, "第一次进入");
			SharePrefUtils.setBoolean(this, "isFirst", false);
			if (ConstantValue.GUIDECOUNT != 0) {
				startActivity(PaperActivity.class);
			}
			finish();
		} else {
			Logger.i(TAG, "第二次次进入");
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
			finish();// 关闭当前的splashactivity .
		}
	}

}
