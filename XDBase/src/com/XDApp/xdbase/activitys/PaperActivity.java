package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.ConstantValue;
import com.XDApp.xdbase.xinsilu.R;

public class PaperActivity extends BaseActivity implements OnPageChangeListener, OnClickListener {

	private static final String TAG = "PaperActivity";
	private ViewPager mViewPager;

	private List<ImageView> imageViewList;

	private int previousSelectPosition = 0;

//	private Button btn_loadui;

	/**
	 * 初始化图片资源文件
	 * 
	 * @return
	 */
	private int[] getImageResIDs() {
		switch (ConstantValue.GUIDECOUNT) {
		case 3:
			return new int[] { R.drawable.guide_page_1, R.drawable.guide_page_2, R.drawable.guide_page_3, };
		case 4:
			return new int[] { R.drawable.guide_page_1, R.drawable.guide_page_2, R.drawable.guide_page_3,
					R.drawable.guide_page_2 };
		case 5:
			return new int[] { R.drawable.guide_page_1, R.drawable.guide_page_2, R.drawable.guide_page_3,
					R.drawable.guide_page_2, R.drawable.guide_page_5 };
		case 6:
			return new int[] { R.drawable.guide_page_1, R.drawable.guide_page_2, R.drawable.guide_page_3,
					R.drawable.guide_page_2, R.drawable.guide_page_5, R.drawable.guide_page_6 };
		default:
			return new int[] { R.drawable.guide_page_1, R.drawable.guide_page_2, R.drawable.guide_page_3 };
		}
	}

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_pager);
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
//		btn_loadui = (Button) findViewById(R.id.btn_loadui);
		prepareData();

	}

	@Deprecated
	private void prepareData() {
		imageViewList = new ArrayList<ImageView>();
		int[] imageResIDs = getImageResIDs();
		// imageDescriptions = getImageDescription();

		ImageView iv;
		for (int i = 0; i < imageResIDs.length; i++) {
			iv = new ImageView(this);
			iv.setBackgroundResource(imageResIDs[i]);
			imageViewList.add(iv);
		}
	}

	@Override
	protected void initEvents() {

		ViewPagerAdapter adapter = new ViewPagerAdapter();
		mViewPager.setAdapter(adapter);
		mViewPager.setOnPageChangeListener(this);
//		btn_loadui.setOnClickListener(this);
	}

	class ViewPagerAdapter extends PagerAdapter {
		@Override
		public int getCount() {
			return imageViewList.size();
		}

		/**
		 * 判断出去的view是否等于进来的view 如果为true直接复用
		 */
		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		/**
		 * 销毁预加载以外的view对象, 会把需要销毁的对象的索引位置传进来就是position
		 */
		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView(imageViewList.get(position));
		}

		/**
		 * 创建一个view
		 */
		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			container.addView(imageViewList.get(position));
			return imageViewList.get(position);
		}
	}

	@Override
	public void onPageScrollStateChanged(int state) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int position) {
		if (position == (imageViewList.size() - 1)) {
			mViewPager.getChildAt(position).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					startActivity(MainActivity.class);
					finish();
				}
			});
			// TODO
			// 显示进入按钮
//			btn_loadui.setVisibility(View.VISIBLE);
		} else {
//			btn_loadui.setVisibility(View.GONE);
		}
		previousSelectPosition = position % imageViewList.size();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
//		case R.id.btn_loadui:
//			startActivity(MainActivity.class);
//			finish();
//			break;
		default:
			break;
		}
	}

}
