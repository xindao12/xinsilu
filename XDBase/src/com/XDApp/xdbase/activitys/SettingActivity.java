package com.XDApp.xdbase.activitys;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.dao.SharePrefUtils;
import com.XDApp.xdbase.dialog.DialogUtil;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.Logger;
import com.XDApp.xdbase.xinsilu.R;

public class SettingActivity extends BaseActivity implements OnClickListener {

	private static final String TAG = null;
	private View rel_setting_push;
	private View rel_setting_autoloading;
	private View rel_setting_checkversion;
	private ImageView iv_setting_push;
	private ImageView iv_setting_autoloading;
	private ImageView iv_app_new;

	private boolean isPush;
	private boolean isAutoLoading;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_setting);
		rel_setting_push = findViewById(R.id.rel_setting_push);
		rel_setting_autoloading = findViewById(R.id.rel_setting_autoloading);
		rel_setting_checkversion = findViewById(R.id.rel_setting_checkversion);
		iv_setting_push = (ImageView) findViewById(R.id.iv_setting_push);
		iv_setting_autoloading = (ImageView) findViewById(R.id.iv_setting_autoloading);
		iv_app_new = (ImageView) findViewById(R.id.iv_app_new);

	}

	@Override
	protected void initEvents() {
		setAppNewView();
		setListener();
	}

	@Override
	protected void onResume() {
		super.onResume();

		isPush = SharePrefUtils.getBoolean(getApplicationContext(), "push", true);
		isAutoLoading = SharePrefUtils.getBoolean(getApplicationContext(), "autoloading", true);

		if (isPush) {
			iv_setting_push.setBackgroundResource(R.drawable.btn_check_on_normal);
			Logger.i(TAG, "显示push");
		} else {
			Logger.i(TAG, "关闭push");
			iv_setting_push.setBackgroundResource(R.drawable.btn_check_off_normal);
		}

		if (isAutoLoading) {
			Logger.i(TAG, "开启autoloading");
			iv_setting_autoloading.setBackgroundResource(R.drawable.btn_check_on_normal);
		} else {
			Logger.i(TAG, "关闭autoloading");
			iv_setting_autoloading.setBackgroundResource(R.drawable.btn_check_off_normal);
		}
	}

	private void setAppNewView() {
		Logger.i(TAG, "检查版本");
		if (mApplication.updateInfo != null
				&& !CommonUtil.getVersion(this).equals(mApplication.updateInfo.getVersion())) {

			iv_app_new.setVisibility(View.VISIBLE);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		SharePrefUtils.setBoolean(getApplicationContext(), "push", isPush);
		SharePrefUtils.setBoolean(getApplicationContext(), "autoloading", isAutoLoading);
		/***************************/
	}

	/**
	 * 设置监听
	 */
	private void setListener() {
		rel_setting_push.setOnClickListener(this);
		rel_setting_autoloading.setOnClickListener(this);
		rel_setting_checkversion.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.rel_setting_push:
			Logger.i(TAG, "更改push");
			setPushChecked();
			break;
		case R.id.rel_setting_autoloading:
			Logger.i(TAG, "更改auto");
			setAutoLoadingChecked();
			break;
		case R.id.rel_setting_checkversion:
			String content = mApplication.updateInfo.getDescription();
			DialogUtil.createDialog(this, null, null, "更新提示", content, "马上更新", "下次再说").show();
			break;
		default:
			break;
		}
	}

	/**
	 * 更改push
	 */
	public void setPushChecked() {
		if (!isPush) {
			iv_setting_push.setBackgroundResource(R.drawable.btn_check_on_normal);
			Logger.i(TAG, "开启push");
			isPush = !isPush;
		} else {
			iv_setting_push.setBackgroundResource(R.drawable.btn_check_off_normal);
			Logger.i(TAG, "关闭push");
			isPush = !isPush;
		}
	}

	/**
	 * 更改autoloading
	 */
	public void setAutoLoadingChecked() {
		if (!isAutoLoading) {
			Logger.i(TAG, "开启autoloading");
			iv_setting_autoloading.setBackgroundResource(R.drawable.btn_check_on_normal);
			isAutoLoading = !isAutoLoading;
		} else {
			Logger.i(TAG, "关闭autoloading");
			iv_setting_autoloading.setBackgroundResource(R.drawable.btn_check_off_normal);
			isAutoLoading = !isAutoLoading;
		}
	}
}
