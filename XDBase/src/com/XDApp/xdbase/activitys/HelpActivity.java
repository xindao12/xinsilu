package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.xinsilu.R;

public class HelpActivity extends BaseActivity {
	private ExpandableListView elv_help;
	private ImageView iv_back;
	private TextView tv_title;
	private List<String> group_list;
	private List<List<String>> item_list2;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_help);
		iv_back = (ImageView) findViewById(R.id.title_back);
		tv_title = (TextView) findViewById(R.id.title_text);

	}

	@Override
	protected void initEvents() {
		group_list = new ArrayList<String>();
		group_list.add("如何注册?");
		group_list.add("如何登录?");
		group_list.add("如何找回密码?");
		group_list.add("如何预订?");
		group_list.add("如何查看/取消订单?");

		List<String> tmp_list1 = new ArrayList<String>();
		tmp_list1.add("点击个人中心页面的“立即登陆”按钮，再点击右上角的“注册”按钮，按照系统提示完成即可。");
		List<String> tmp_list2 = new ArrayList<String>();
		tmp_list2.add("点击个人中心页面的“立即登陆”按钮，再输入用户名及密码即可。");
		List<String> tmp_list3 = new ArrayList<String>();
		tmp_list3.add("点击个人中心页面的“立即登陆”按钮，再点击登录按钮右下角的“忘记密码”，按照系统提示完成即可。");
		List<String> tmp_list4 = new ArrayList<String>();
		tmp_list4.add("首先请您注册登录，然后在产品详情页面点击“预订”按钮，进入预定界面，请选择出行日期（预订特价商城产品无需选择），再填好您的姓名及电话，点击“提交订单”按钮即可。");
		List<String> tmp_list5 = new ArrayList<String>();
		tmp_list5.add("首先请您登录，然后进入订单页面，点击您想查看的订单类别，选择您想查看的订单即可。");

		item_list2 = new ArrayList<List<String>>();
		item_list2.add(tmp_list1);
		item_list2.add(tmp_list2);
		item_list2.add(tmp_list3);
		item_list2.add(tmp_list4);
		item_list2.add(tmp_list5);

		iv_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		tv_title.setText("帮助");
		elv_help = (ExpandableListView) findViewById(R.id.elv_help);
		MyAdapter adapter = new MyAdapter();
		elv_help.setAdapter(adapter);

	}

	private class MyAdapter extends BaseExpandableListAdapter {

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return item_list2.get(groupPosition).get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			ItemHolder itemHolder = null;
			if (convertView == null) {
				convertView = View.inflate(HelpActivity.this, R.layout.expendlist_item, null);
				itemHolder = new ItemHolder();
				itemHolder.txt = (TextView) convertView.findViewById(R.id.txt);
				itemHolder.img = (ImageView) convertView.findViewById(R.id.img);
				convertView.setTag(itemHolder);
			} else {
				itemHolder = (ItemHolder) convertView.getTag();
			}
			itemHolder.txt.setText(item_list2.get(groupPosition).get(childPosition));
			return convertView;

		}

		@Override
		public int getChildrenCount(int groupPosition) {
			return item_list2.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			return group_list.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			return group_list.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			GroupHolder groupHolder = null;
			if (convertView == null) {
				convertView = View.inflate(HelpActivity.this, R.layout.expendlist_group, null);
				groupHolder = new GroupHolder();
				groupHolder.txt = (TextView) convertView.findViewById(R.id.group_txt);
				// groupHolder.img =
				// (ImageView)convertView.findViewById(R.id.img);
				convertView.setTag(groupHolder);
			} else {
				groupHolder = (GroupHolder) convertView.getTag();
			}
			groupHolder.txt.setText(group_list.get(groupPosition));
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}
	}

	class GroupHolder {
		public TextView txt;
		public ImageView img;
	}

	class ItemHolder {
		public ImageView img;
		public TextView txt;
	}

}
