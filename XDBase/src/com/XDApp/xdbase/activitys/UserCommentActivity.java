package com.XDApp.xdbase.activitys;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.XDApp.xdbase.BaseActivity;
import com.XDApp.xdbase.bean.Comment;
import com.XDApp.xdbase.utils.CommonUtil;
import com.XDApp.xdbase.utils.XinDaoBaseAdapter;
import com.XDApp.xdbase.xinsilu.R;

public class UserCommentActivity extends BaseActivity {
	private ListView lv_comment;
	private List<Comment> list;
	private TextView title;
	private ImageView back;

	@Override
	protected void initViews() {
		setContentView(R.layout.activity_commentlist);
		lv_comment = (ListView) findViewById(R.id.lv_comment);
		title = (TextView) findViewById(R.id.title_text);
		back = (ImageView) findViewById(R.id.title_back);
	}

	@Override
	protected void initEvents() {
		title.setText("用户点评");
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		list = new ArrayList<Comment>();
//		list = (List<Comment>) getIntent().getParcelableArrayListExtra("commentList").get(0);

		CommentAdapter adapter = new CommentAdapter(UserCommentActivity.this, list, 100,
				R.layout.item_detail_comment_, R.layout.item_loading);
		lv_comment.setAdapter(adapter);
	}

	/**
	 * 评论的适配器
	 * @author Simba
	 *
	 */
	class CommentAdapter extends XinDaoBaseAdapter<Comment> {

		public CommentAdapter(Context context, List<Comment> iniData, int pageSize, int res, int loadingRes) {
			super(context, iniData, pageSize, res, loadingRes);
		}

		class ViewHolder {
			TextView name;
		}

		@Override
		public View getView(int position, View contentView, ViewGroup parent, Comment t) {
			View view = View.inflate(mContext, R.layout.item_detail_comment_, null);
			TextView name = (TextView) view.findViewById(R.id.title);
			TextView content = (TextView) view.findViewById(R.id.content);
			TextView time = (TextView) view.findViewById(R.id.time);
			name.setText(t.getNickName());
			content.setText(t.getContext());
			time.setText(CommonUtil.formatTime(Integer.parseInt(t.getTime()) * 1000, true));
			return view;
		}

		@Override
		public void nextPage(int start, int size,
				com.XDApp.xdbase.utils.XinDaoBaseAdapter.ILoadNextPageData<Comment> iLoadNextPageData) {

		}
	}
}
