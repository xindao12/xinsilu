package com.XDApp.xdbase;

// Static wrapper library around AsyncHttpClient

import com.XDApp.xdbase.utils.CommonLog;
import com.XDApp.xdbase.utils.xdConfig;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class XDHttpClient {
	protected static CommonLog log = new CommonLog();
	private static AsyncHttpClient client = new AsyncHttpClient();

	public static void get(String api, RequestParams params, AsyncHttpResponseHandler responseHandler) {

		String absoluteUrl = getAbsoluteUrl(api);
		if (absoluteUrl.indexOf("?") != -1) {
			log.i(getAbsoluteUrl(api) + "&" + params.toString());
		} else {
			log.i(getAbsoluteUrl(api) + "?" + params.toString());
		}

		client.get(getAbsoluteUrl(api), params, responseHandler);
	}

	public static void post(String api, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		log.i(getAbsoluteUrl(api) + params.toString());
		client.post(getAbsoluteUrl(api), params, responseHandler);
	}

	public static String getAbsoluteUrl(String relativeUrl) {
		return xdConfig.HOST_URL + relativeUrl;
	}

}
