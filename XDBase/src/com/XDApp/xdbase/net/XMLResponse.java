package com.XDApp.xdbase.net;

public class XMLResponse {

  public static final int RESPONSE_OK = 99;
  public static final int RESPONSE_VERIFY_USER = 41;
  public static final int RESPONSE_SERVER_EXCEPTION = 44;
  public static final int RESPONSE_PROFESSION_ERROR = 49;

  public static String getReasonPhrase(int reason) {
    if (reason == RESPONSE_OK) return "请求成功";
    if (reason == RESPONSE_PROFESSION_ERROR) return "业务请求失败";
    if (reason == RESPONSE_SERVER_EXCEPTION) return "服务器数据错误";
    if (reason == RESPONSE_VERIFY_USER) return "用户验证失败";
    return "未知原因";
  }

}
