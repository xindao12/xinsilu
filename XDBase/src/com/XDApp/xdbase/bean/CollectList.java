package com.XDApp.xdbase.bean;

public class CollectList {
	/** 标题*/
	private String title;
	/** 景点ID*/
	private String seneryId;
	/** 介绍    
	 *   自由行，参团是简介，门票是地址*/
	private String introduce;
	/** 图片url*/
	private String url;
	/** 市场价*/
	private String normalPrice;
	/** 会员价*/
	private String vipPrice;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSeneryId() {
		return seneryId;
	}

	public void setSeneryId(String seneryId) {
		this.seneryId = seneryId;
	}

	public String getIntroduce() {
		return introduce;
	}

	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNormalPrice() {
		return normalPrice;
	}

	public void setNormalPrice(String normalPrice) {
		this.normalPrice = normalPrice;
	}

	public String getVipPrice() {
		return vipPrice;
	}

	public void setVipPrice(String vipPrice) {
		this.vipPrice = vipPrice;
	}

}
