package com.XDApp.xdbase.bean;

import java.util.List;

/**
 * 详情价格信息
 * @author Simba
 *
 */
public class TicketPriceInfo {
	public List<BargainPriceTicketInfo> ticketList;
	public String content;
}
