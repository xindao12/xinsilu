package com.XDApp.xdbase.bean;

public class PicUrl {
	private String[] imgUrl;
	private String url;
	private String alt;
	public String[] getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String[] imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getAlt() {
		return alt;
	}
	public void setAlt(String alt) {
		this.alt = alt;
	}
}
