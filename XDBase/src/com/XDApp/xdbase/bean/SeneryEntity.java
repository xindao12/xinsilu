package com.XDApp.xdbase.bean;

import java.util.List;

public class SeneryEntity {
	private String seneryList;
	private List<Comment>list;
	private List<PicUrl> picURLs;//轮播图地址
	private List<Order>orderList;//价格
	private String orderTitle;
	public List<Order> getOrderList() {
		return orderList;
	}

	public String getOrderTitle() {
		return orderTitle;
	}

	public void setOrderTitle(String orderTitle) {
		this.orderTitle = orderTitle;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	private String costs;
	private String importantTips;
	private String productFeatures;
	private String tripIntroduce;
	
	public List<Comment> getList() {
		return list;
	}

	public void setList(List<Comment> list) {
		this.list = list;
	}

	public List<PicUrl> getPicURLs() {
		return picURLs;
	}

	public void setPicURLs(List<PicUrl> picURLs) {
		this.picURLs = picURLs;
	}

	public String getCosts() {
		return costs;
	}

	public void setCosts(String costs) {
		this.costs = costs;
	}

	public String getImportantTips() {
		return importantTips;
	}

	public void setImportantTips(String importantTips) {
		this.importantTips = importantTips;
	}

	public String getProductFeatures() {
		return productFeatures;
	}

	public void setProductFeatures(String productFeatures) {
		this.productFeatures = productFeatures;
	}

	public String getTripIntroduce() {
		return tripIntroduce;
	}

	public void setTripIntroduce(String tripIntroduce) {
		this.tripIntroduce = tripIntroduce;
	}

	public String getSeneryList() {
		return seneryList;
	}

	public void setSeneryList(String seneryList) {
		this.seneryList = seneryList;
	}

	
	
	
}
