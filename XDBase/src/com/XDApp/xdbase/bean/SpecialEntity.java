package com.XDApp.xdbase.bean;

import java.util.List;

public class SpecialEntity {
	private List<Product> productList;

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
}
