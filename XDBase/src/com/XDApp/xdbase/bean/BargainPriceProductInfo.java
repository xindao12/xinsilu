package com.XDApp.xdbase.bean;

/**
 * 特价商品信息
 * @author Simba
 *
 */
public class BargainPriceProductInfo {
	public String productId;
	public String productTitle;
	public String vipPrice;
}
