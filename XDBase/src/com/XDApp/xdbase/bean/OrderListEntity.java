package com.XDApp.xdbase.bean;

import java.util.List;

public class OrderListEntity extends BaseEntity {

	private int totalCount;

	private List<OrderList> orderList;

	public int getTotalConunt() {
		return totalCount;
	}

	public void setTotalConunt(int totalConunt) {
		this.totalCount = totalConunt;
	}

	public List<OrderList> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderList> data) {
		this.orderList = data;
	}

}
