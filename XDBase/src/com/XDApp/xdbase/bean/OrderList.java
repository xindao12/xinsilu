package com.XDApp.xdbase.bean;

public class OrderList {
	private String orderId;
	private String orderTime;
	private String orderName;
	private String orderPrice;
	private String orderPalyTime;
	private int nums;

	public int getNums() {
		return nums;
	}

	public void setNums(int nums) {
		this.nums = nums;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public String getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getOrderPalyTime() {
		return orderPalyTime;
	}

	public void setOrderPalyTime(String orderPalyTime) {
		this.orderPalyTime = orderPalyTime;
	}

}
