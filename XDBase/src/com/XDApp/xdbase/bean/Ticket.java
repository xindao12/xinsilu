package com.XDApp.xdbase.bean;
public class Ticket {
	private String ticketId;
	private String seneryTitle;
	private String seneryLocation;
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getSeneryTitle() {
		return seneryTitle;
	}
	public void setSeneryTitle(String seneryTitle) {
		this.seneryTitle = seneryTitle;
	}
	public String getSeneryLocation() {
		return seneryLocation;
	}
	public void setSeneryLocation(String seneryLocation) {
		this.seneryLocation = seneryLocation;
	}
	public String getSeneryUrl() {
		return seneryUrl;
	}
	public void setSeneryUrl(String seneryUrl) {
		this.seneryUrl = seneryUrl;
	}
	public String getNormalPrice() {
		return normalPrice;
	}
	public void setNormalPrice(String normalPrice) {
		this.normalPrice = normalPrice;
	}
	public String getVipPrice() {
		return vipPrice;
	}
	public void setVipPrice(String vipPrice) {
		this.vipPrice = vipPrice;
	}
	private String seneryUrl;
	private String normalPrice;
	private String vipPrice;
	
}
