package com.XDApp.xdbase.bean;

import java.util.List;

public class StoreEntity extends BaseEntity {
	private int totalCount;
	private List<CollectList> list;

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<CollectList> getList() {
		return list;
	}

	public void setList(List<CollectList> list) {
		this.list = list;
	}

}
