package com.XDApp.xdbase.bean;

public class FreeWalker {
	private String normalPrice;
	private String vipPrice;
	private String title;
	private String seneryId;
	public String getSeneryId() {
		return seneryId;
	}
	public void setSeneryId(String seneryId) {
		this.seneryId = seneryId;
	}
	private String url;
	private String introduce;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getNormalPrice() {
		return normalPrice;
	}
	public void setNormalPrice(String normalPrice) {
		this.normalPrice = normalPrice;
	}
	public String getVipPrice() {
		return vipPrice;
	}
	public void setVipPrice(String vipPrice) {
		this.vipPrice = vipPrice;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
