package com.XDApp.xdbase.bean;

import java.util.List;

public class FreeWalkerEntity extends BaseEntity {
	private List<FreeWalker> list;
	private String totalCount;

	public String getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

	public List<FreeWalker> getList() {
		return list;
	}

	public void setList(List<FreeWalker> list) {
		this.list = list;
	}
	
}
