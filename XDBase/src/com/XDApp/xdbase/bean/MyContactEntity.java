package com.XDApp.xdbase.bean;

import java.util.List;

public class MyContactEntity {
	private List<Contact> list;

	public List<Contact> getList() {
		return list;
	}

	public void setList(List<Contact> list) {
		this.list = list;
	}
}
