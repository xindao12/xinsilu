package com.XDApp.xdbase.bean;

import java.util.List;

/**
 * 详情价格信息
 * @author Simba
 *
 */
public class DetailPriceInfo {
	public List<BargainPriceProductInfo> productList;
	public String content;
}
