package com.XDApp.xdbase.bean;

import java.util.List;

public class CommentListEntity extends BaseEntity {
	private List<CommentList> list;

	public List<CommentList> getList() {
		return list;
	}

	public void setList(List<CommentList> list) {
		this.list = list;
	}

}
